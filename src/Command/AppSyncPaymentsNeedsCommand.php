<?php

namespace App\Command;

use App\Entity\Transaction;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppSyncPaymentsNeedsCommand extends Command
{
    protected static $defaultName = 'app:sync-payments-needs';

    private $container;

    public function __construct($name = null, ContainerInterface $container)
    {
        parent::__construct($name);
        $this->container = $container;
    }


    protected function configure()
    {
        $this->setDescription('Sync payments for renter system');
        //$this->transactionnRepository =  $this->getContainer()->get('doctrine')->getManager();

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->text("Timestamp Process Start :".date('Y-m-d H:i:s'));
        $io->newLine();

        $this->checkRental($io);
        $this->checkUtilityDeposit($io);
        $this->checkRentDeposit($io);

        $io->newLine();
        $io->text("Timestamp Process End :".date('Y-m-d H:i:s'));
    }


    private function checkPastRental(SymfonyStyle $io){
        $rentText = "SEWA";
        $rentPeriodText = "BULAN";
        $ownerText = "Pemilik";
        $tenantText = "Penyewa"; 
       


        $entityManager = $this->container->get('doctrine')->getManager();
         
         /** Check Monthly Rentals **/
        //Get all active contracts
        $q = $entityManager->createQuery(
                'select c,per,pla
                from App\Entity\Contract c
                join c.person per
                join c.place pla
                where :now BETWEEN c.startDate AND c.endDate
                and 
                (
                    c.actualEndDate is null
                    or
                    (
                        MONTH(c.actualEndDate) = MONTH(:now)
                        AND
                        YEAR(c.actualEndDate) = YEAR(:now)
                    )
                )');
        $q->setParameter(':now',date('Y-m-d H:i:s'));
        $activeContracts = $q->getResult();

        $io->title("Check For All Active Contract Holders");
        //Check each contract
        $i=0;
        foreach($activeContracts as $contract){
            $i++;
            $current_person = $contract->getPerson();
            $current_person_class = str_replace("App\Entity\\","",$current_person->getClass());
            if($current_person_class == "Tenant"){
                $personText = $tenantText;
            }else{
                $personText = $ownerText;
            }
            $current_location = $contract->getPlace();

            $io->section($i."# Contract Holder");

            if($contract->getActualEndDate()){
                $contractActualEndDate = $contract->getActualEndDate()->format('d-m-Y');
            }else{
                $contractActualEndDate = " ------ ";
            }


            $io->text(array(
                $current_person_class." Name: ".$current_person->getName(),
                "Contract Start Date :".$contract->getStartDate()->format('d-m-Y'),
                "Contract Actual End Date :".$contractActualEndDate,
                "Contract End Date :".$contract->getEndDate()->format('d-m-Y'),
                "Rent Amount : MYR".$contract->getRentAmount()
            ));

            $durationUpToCurrent = date_diff(new Datetime("now"),$contract->getStartDate());
            
            $currentCheckMonth = clone $durationUpToCurrent;

            //Check whether tenant have transaction record prior before command execution.
            $q = $entityManager->createQuery(
                    'select t
                    from App\Entity\Transaction t
                    join t.person per
                    where t.transactionDescription like :rentFormat
                    and per.personId = :personId');
            $q->setParameter("personId",$current_person->getPersonId());
            $q->setParameter("rentFormat",$rentText." ".$current_location->getNumber()." ".date("m-Y")." ".$personText."%");
            $rentTransactions = $q->getResult();
            $io->newLine();
            //If not found then create new rent transaction
            if(count($rentTransactions) == 0){
                $io->text(array("Rental Record Not Found"));
                //If Actual End Date On Same Month
                if($contract->getActualEndDate->format('m-Y') == date('m-Y')){
                    $daysOfCurrentMonth = cal_days_in_month(CAL_GREGORIAN,date('m'),date('Y'));
                    $rentPerDay = $contract->getRentAmount() / $daysOfCurrentMonth;
                    $rentAmount = $rentPerDay * date('d');
                }else{
                    $rentAmount = $contract->getRentAmount();
                }
                $currentMonthRentTransaction = new Transaction();
                $currentMonthRentTransaction->setTransactionDescription($rentText." ".$current_location->getNumber()." ".date("m-Y")." ".$personText);
                $currentMonthRentTransaction->setTransactionAmount($rentAmount);
                if($contract->getContractType() == "RoomRentAgreement"){
                    $currentMonthRentTransaction->setTransactionType("DEBIT");
                }else{
                    $currentMonthRentTransaction->setTransactionType("CREDIT");
                }
                $currentMonthRentTransaction->setStatus("PENDING");
                $currentMonthRentTransaction->setDateIssued(new \Datetime("now"));
                $currentMonthRentTransaction->setPerson($current_person);

                $io->text(array(
                    "Adding New Transacion Record :",
                "Transaction Description: ".$currentMonthRentTransaction->getTransactionDescription(),
                "Transaction Amount: MYR".$currentMonthRentTransaction->getTransactionAmount(),
                "Transaction Type: ".$currentMonthRentTransaction->getTransactionType(),
                "Transaction Status: ".$currentMonthRentTransaction->getStatus(),
                "Person Name And Id Associated: ".$currentMonthRentTransaction->getPerson()->getIcAndName()
                ));

                $entityManager->persist($currentMonthRentTransaction);
                $entityManager->flush();
            }else{
                $io->text(array("Rental Record Exist in Database"));
                foreach($rentTransactions as $rentTransaction){
                    $io->text(array(
                        "Transaction ID: ".$rentTransaction->getId(),
                        "Transaction Description: ".$rentTransaction->getTransactionDescription()
                    ));
                }
            }
            $io->newLine(3);
        }
    }

    private function checkRental(SymfonyStyle $io){

        $rentText = "SEWA";
        $rentPeriodText = "BULAN";
        $ownerText = "Pemilik";
        $tenantText = "Penyewa"; 
       


        $entityManager = $this->container->get('doctrine')->getManager();
         
         /** Check Monthly Rentals **/
        //Get all active contracts
        $q = $entityManager->createQuery(
                'select c,per,pla
                from App\Entity\Contract c
                join c.person per
                join c.place pla
                where :now BETWEEN c.startDate AND c.endDate
                and 
                (
                    c.actualEndDate is null
                    or
                    (
                        MONTH(c.actualEndDate) = MONTH(:now)
                        AND
                        YEAR(c.actualEndDate) = YEAR(:now)
                    )
                )');
        $q->setParameter(':now',date('Y-m-d H:i:s'));
        $activeContracts = $q->getResult();

        $io->title("Check For All Active Contract Holders");
        //Check each contract
        $i=0;
        foreach($activeContracts as $contract){
            $i++;
            $current_person = $contract->getPerson();
            $current_person_class = str_replace("App\Entity\\","",$current_person->getClass());
            if($current_person_class == "Tenant"){
                $personText = $tenantText;
            }else{
                $personText = $ownerText;
            }
            $current_location = $contract->getPlace();

            $io->section($i."# Contract Holder");

            if($contract->getActualEndDate()){
                $contractActualEndDate = $contract->getActualEndDate()->format('d-m-Y');
            }else{
                $contractActualEndDate = " ------ ";
            }

            $io->text(array(
                $current_person_class." Name: ".$current_person->getName(),
                "Contract Start Date :".$contract->getStartDate()->format('d-m-Y'),
                "Contract Actual End Date :". $contractActualEndDate,
                "Contract End Date :".$contract->getEndDate()->format('d-m-Y'),
                "Rent Amount : MYR".$contract->getRentAmount()
            ));            

            //Check whether tenant have transaction record prior before command execution.
            $q = $entityManager->createQuery(
                    'select t
                    from App\Entity\Transaction t
                    join t.person per
                    where t.transactionDescription like :rentFormat
                    and per.personId = :personId');
            $q->setParameter("personId",$current_person->getPersonId());
            $q->setParameter("rentFormat",$rentText." ".$current_location->getNumber()." ".date("m-Y")." ".$personText."%");
            $rentTransactions = $q->getResult();
            $io->newLine();
            //If not found then create new rent transaction
            if(count($rentTransactions) == 0){
                $io->text(array("Rental Record Not Found"));
                $currentMonthRentTransaction = new Transaction();
                $currentMonthRentTransaction->setTransactionDescription($rentText." ".$current_location->getNumber()." ".date("m-Y")." ".$personText);
                $currentMonthRentTransaction->setTransactionAmount($contract->getRentAmount());
                if($contract->getContractType() == "RoomRentAgreement"){
                    $currentMonthRentTransaction->setTransactionType("DEBIT");
                }else{
                    $currentMonthRentTransaction->setTransactionType("CREDIT");
                }
                $currentMonthRentTransaction->setStatus("PENDING");
                $currentMonthRentTransaction->setDateIssued(new \Datetime("now"));
                $currentMonthRentTransaction->setPerson($current_person);

                $io->text(array(
                    "Adding New Transacion Record :",
                "Transaction Description: ".$currentMonthRentTransaction->getTransactionDescription(),
                "Transaction Amount: MYR".$currentMonthRentTransaction->getTransactionAmount(),
                "Transaction Type: ".$currentMonthRentTransaction->getTransactionType(),
                "Transaction Status: ".$currentMonthRentTransaction->getStatus(),
                "Person Name And Id Associated: ".$currentMonthRentTransaction->getPerson()->getIcAndName()
                ));

                $entityManager->persist($currentMonthRentTransaction);
                $entityManager->flush();
            }else{
                $io->text(array("Rental Record Exist in Database"));
                foreach($rentTransactions as $rentTransaction){
                    $io->text(array(
                        "Transaction ID: ".$rentTransaction->getId(),
                        "Transaction Description: ".$rentTransaction->getTransactionDescription()
                    ));
                }
            }
            $io->newLine(3);
        }
    }

    private function checkUtilityDeposit(SymfonyStyle $io){
        /** Check For Returning Utility Deposits **/
        $utilityDepositText = "UTILITI_DEPOSIT";
        $ownerText = "Pemilik";
        $tenantText = "Penyewa";

        $io->title("Check For Utility Returning Deposit");

        $entityManager = $this->container->get('doctrine')->getManager();

        $q = $entityManager->createQuery(
                'select c,per,pla
                from App\Entity\Contract c
                join c.person per
                join c.place pla
                where c.actualEndDate is not null');
        $endedContracts = $q->getResult();
        
        $i=0;
        foreach($endedContracts as $contract){
            $i++;
            $nextdayEndOfContract = clone $contract->getActualEndDate();
            $nextdayEndOfContract->modify('+1 day');

            $current_person = $contract->getPerson();
            $current_person_class = str_replace("App\Entity\\","",$current_person->getClass());
            if($current_person_class == "Tenant"){
                $personText = $tenantText;
            }else{
                $personText = $ownerText;
            }
            $current_location = $contract->getPlace();

            $io->section($i."# Contract Holder");

            if($contract->getActualEndDate()){
                $contractActualEndDate = $contract->getActualEndDate()->format('d-m-Y');
            }else{
                $contractActualEndDate = " ------ ";
            }

            $io->text(array(
                $current_person_class." Name: ".$current_person->getName(),
                "Contract Start Date :".$contract->getStartDate()->format('d-m-Y'),
                "Contract Actual End Date :".$contractActualEndDate,
                "Contract End Date :".$contract->getEndDate()->format('d-m-Y'),
                "Actual End Date:".$contract->getActualEndDate()->format('d-m-Y'),
                "Utility Deposit: MYR".$contract->getRentUtility(),
                "Rent Deposit: MYR".$contract->getRentDeposit(),
            ));

            //Check for existing utility deposit
            $io->text(array("Check Utility Deposit"));
            $q = $entityManager->createQuery(
                    'select t
                    from App\Entity\Transaction t
                    join t.person per
                    where t.transactionDescription like :utilityDepositFormat
                    and per.personId = :personId');
            $q->setParameter("personId",$current_person->getPersonId());
            $q->setParameter("utilityDepositFormat",
                $utilityDepositText
                ." ".$current_location->getNumber()
                ." ".$contract->getEndDate()->format('d-m-Y')
                ." ".$personText."%"
            );
            $utilityDepositTransactions = $q->getResult();
            $io->newLine();

            //If not found
            if(count($utilityDepositTransactions) == 0){
                $io->text(array("Utility Deposit Record Not Found"));

                $currentUtilityTransaction = new Transaction();
                $currentUtilityTransaction->setTransactionDescription(
                    $utilityDepositText
                    ." ".$current_location->getNumber()
                    ." ".$contract->getEndDate()->format('d-m-Y')
                    ." ".$personText
                );
                $currentUtilityTransaction->setTransactionAmount($contract->getRentUtility());
                if($contract->getContractType() == "RoomRentAgreement"){
                     $currentUtilityTransaction->setTransactionType("CREDIT");
                }else{
                     $currentUtilityTransaction->setTransactionType("DEBIT");
                }
                $currentUtilityTransaction->setStatus("PENDING");
                $currentUtilityTransaction->setDateIssued($nextdayEndOfContract);
                $currentUtilityTransaction->setPerson($current_person);

                $io->text(array(
                    "Adding New Transacion Record :",
                    "Transaction Description: ".$currentUtilityTransaction->getTransactionDescription(),
                    "Transaction Amount: MYR".$currentUtilityTransaction->getTransactionAmount(),
                    "Transaction Type: ".$currentUtilityTransaction->getTransactionType(),
                    "Transaction Status: ".$currentUtilityTransaction->getStatus(),
                    "Person Name And Id Associated: ".$currentUtilityTransaction->getPerson()->getIcAndName()
                ));

                $entityManager->persist($currentUtilityTransaction);
                $entityManager->flush();

            }else{
                $io->text(array("Utility Deposit Record Exist in Database"));
                foreach($utilityDepositTransactions as $utilityDepositTransaction){
                    $io->text(array(
                        "Transaction ID: ".$utilityDepositTransaction->getId(),
                        "Transaction Description: ".$utilityDepositTransaction->getTransactionDescription()
                    ));
                }
            }
        }
    }

    public function checkRentDeposit(SymfonyStyle $io){
        /** Check For Returning Utility Deposits **/
        $rentDepositText = "SEWA_DEPOSIT";
        $ownerText = "Pemilik";
        $tenantText = "Penyewa";

        $io->title("Check For Returning Rent Deposit");

        $entityManager = $this->container->get('doctrine')->getManager();

        $q = $entityManager->createQuery(
                'select c,per,pla
                from App\Entity\Contract c
                join c.person per
                join c.place pla
                where c.actualEndDate is not null');
        $endedContracts = $q->getResult();
        
        $i=0;
        foreach($endedContracts as $contract){
            $reason = null;
            $i++;
            $nextdayEndOfContract = clone $contract->getActualEndDate();
            $nextdayEndOfContract->modify('+1 day');

            $current_person = $contract->getPerson();
            $current_person_class = str_replace("App\Entity\\","",$current_person->getClass());
            if($current_person_class == "Tenant"){
                $personText = $tenantText;
            }else{
                $personText = $ownerText;
            }
            $current_location = $contract->getPlace();

            $io->section($i."# Contract Holder");

            if($contract->getActualEndDate()){
                $contractActualEndDate = $contract->getActualEndDate()->format('d-m-Y');
            }else{
                $contractActualEndDate = " ------ ";
            }

            $io->text(array(
                $current_person_class." Name: ".$current_person->getName(),
                "Contract Start Date :".$contract->getStartDate()->format('d-m-Y'),
                "Contract Actual End Date :".$contractActualEndDate,
                "Contract End Date :".$contract->getEndDate()->format('d-m-Y'),
                "Actual End Date:".$contract->getActualEndDate()->format('d-m-Y'),
                "Utility Deposit: MYR".$contract->getRentUtility(),
                "Rent Deposit: MYR".$contract->getRentDeposit(),
            ));


            $io->text("Check Rent Deposit");
            $q = $entityManager->createQuery(
                    'select t
                    from App\Entity\Transaction t
                    join t.person per
                    where t.transactionDescription like :rentDepositFormat
                    and per.personId = :personId');
            $q->setParameter("personId",$current_person->getPersonId());
            $q->setParameter("rentDepositFormat",
                $rentDepositText
                ." ".$current_location->getNumber()
                ." ".$contract->getEndDate()->format('d-m-Y')
                ." ".$personText."%"
            );
            $rentDepositTransactions = $q->getResult();
            $io->newLine();

            if(count($rentDepositTransactions) == 0){
                $io->text(array("Rent Deposit Record Not Found"));

                if($contract->getEndDate() == $contract->getActualEndDate()){
                    //If on same contract actual end date and end date
                    $currentRentDepositTransaction = new Transaction();
                    $currentRentDepositTransaction->setTransactionDescription(
                        $rentDepositText
                        ." ".$current_location->getNumber()
                        ." ".$contract->getEndDate()->format('d-m-Y')
                        ." ".$personText
                    );
                    $currentRentDepositTransaction->setTransactionAmount($contract->getRentDeposit());
                    if($contract->getContractType() == "RoomRentAgreement"){
                        $currentRentDepositTransaction->setTransactionType("CREDIT");
                    }else{
                        $currentRentDepositTransaction->setTransactionType("DEBIT");
                    }
                    $currentRentDepositTransaction->setStatus("PENDING");
                    $currentRentDepositTransaction->setDateIssued($nextdayEndOfContract);
                    $currentRentDepositTransaction->setPerson($current_person);

                    $io->text(array(
                        "Adding New Transacion Record :",
                        "Transaction Description: ".$currentRentDepositTransaction->getTransactionDescription(),
                        "Transaction Amount: MYR".$currentRentDepositTransaction->getTransactionAmount(),
                        "Transaction Type: ".$currentRentDepositTransaction->getTransactionType(),
                        "Transaction Status: ".$currentRentDepositTransaction->getStatus(),
                        "Person Name And Id Associated: ".$currentRentDepositTransaction->getPerson()->getIcAndName()
                    ));

                    $entityManager->persist($currentRentDepositTransaction);
                    $entityManager->flush();
                }elseif($contract->getEndDate() < $contract->getActualEndDate()){
                    //If actual end date is before end date
                   
                }
                else{
                     $io->text("Tenant left before end of contract");
                }
            }else{
               $io->text(array("Rent Deposit Record Exist in Database"));
                foreach($rentDepositTransactions as $rentDepositTransaction){
                    $io->text(array(
                        "Transaction ID: ".$rentDepositTransaction->getId(),
                        "Transaction Description: ".$rentDepositTransaction->getTransactionDescription()
                    ));
                }
            }
        }   
    }
}


<?php

namespace App\Controller;

use App\Entity\Contract;
use App\Form\TenantContractType;
use App\Form\OwnerContractType;
use App\Repository\ContractRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/contract")
 */
class ContractController extends Controller
{
    /**
     * @Route("/", name="contract_index", methods="GET")
     */
    public function index(ContractRepository $contractRepository): Response
    {
        return $this->render('contract/index.html.twig', ['contracts' => $contractRepository->findAll()]);
    }

    /**
     * @Route("/new", name="contract_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $contract = new Contract();
        $form = $this->createForm(ContractType::class, $contract);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contract);
            $em->flush();

            return $this->redirectToRoute('contract_index');
        }

        return $this->render('contract/new.html.twig', [
            'contract' => $contract,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contract_show", methods="GET")
     */
    public function show(Contract $contract): Response
    {
        return $this->render('contract/show.html.twig', ['contract' => $contract]);
    }

    /**
     * @Route("/{id}/edit", name="contract_edit", methods="GET|POST")
     */
    public function edit(Request $request, Contract $contract): Response
    {

        $em = $this->getDoctrine()->getManager();
    
        if($contract->getPerson()->getClass() == "App\Entity\Tenant"){
            $form = $this->createForm(TenantContractType::class, null);
        }elseif($contract->getPerson()->getClass() == "App\Entity\Owner"){
            $form = $this->createForm(OwnerContractType::class, null);
        }
        $form->handleRequest($request);

        $data = $form->getData();

        if ($form->isSubmitted() && $form->isValid()) {

            $contract->setStartDate($data["startDate"]);
            $contract->setEndDate($data["endDate"]);
            $contract->setRentAmount($data["rentAmount"]);
            $contract->setRentFrequency($data["rentFrequency"]);
            $contract->setRentDeposit($data["rentDeposit"]);
            $contract->setRentUtility($data["rentUtility"]);
            $contract->setActualEndDate($data["actualEndDate"]);
            $contract->setRenewal($data["renewal"]);
            if($contract->getPerson()->getClass() == "App\Entity\Tenant"){
               $contract->setPlace($data["room"]);
               $contract->setPerson($data["tenant"]);
            }elseif($contract->getPerson()->getClass() == "App\Entity\Owner"){
               $contract->setPlace($data["home"]);
               $contract->setPerson($data["owner"]);
            }

            $em->persist($contract);
            $em->flush();

            return $this->redirectToRoute('contract_edit', ['id' => $contract->getId()]);
        }

            $form->get("startDate")->setData($contract->getStartDate());
            $form->get("endDate")->setData($contract->getEndDate());
            $form->get("rentAmount")->setData($contract->getRentAmount());
            $form->get("rentFrequency")->setData($contract->getRentFrequency());
            $form->get("rentDeposit")->setData($contract->getRentDeposit());
            $form->get("rentUtility")->setData($contract->getRentUtility());
            $form->get("actualEndDate")->setData($contract->getActualEndDate());
              if($contract->getPerson()->getClass() == "App\Entity\Tenant"){
                $form->get("tenant")->setData($contract->getPerson());
                $form->get("room")->setData($contract->getPlace());

            }elseif($contract->getPerson()->getClass() == "App\Entity\Owner"){
                $form->get("owner")->setData($contract->getPerson());
                $form->get("home")->setData($contract->getPlace());
            }
            $form->get("renewal")->setData($contract->getRenewal());


        return $this->render('contract/edit.html.twig', [
            'contract' => $contract,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contract_delete", methods="DELETE")
     */
    public function delete(Request $request, Contract $contract): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contract->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contract);
            $em->flush();
        }

        return $this->redirectToRoute('contract_index');
    }
}

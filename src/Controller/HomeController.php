<?php

namespace App\Controller;

use App\Entity\Home;
use App\Entity\Room;
use App\Entity\Owner;
use App\Entity\Contract;
use App\Form\HomeType;
use App\Form\PersonType;
use App\Form\ContractType;
use App\Form\NewHouseFormType;
use App\Form\RenewHomeContractFormType;
use App\Repository\HomeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use PhpOffice\PhpWord\TemplateProcessor;
use Omines\DataTablesBundle\Adapter\ArrayAdapter;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\Controller\DataTablesTrait;
use Doctrine\ORM\QueryBuilder;


/**
 * @Route("/home")
 */
class HomeController extends Controller
{
    use DataTablesTrait;
    /**
     * @Route("/", name="home_index", methods="GET")
     */
    public function index(HomeRepository $homeRepository): Response
    {  

        return $this->render('home/index.html.twig', [
            'homes' => $homeRepository->findAll(),
            'home_link' => true,
            "home_index" => true 
        ]);
    }

    /**
     * @Route("/new", name="home_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $home = new Home();
        $owner = new Owner();
        $contract = new Contract();

        $form = $this->createForm(NewHouseFormType::class, null);
        $form->handleRequest($request);

        $data = $form->getData();
        
        if($form->isSubmitted() && $form->isValid()){
            
            $em = $this->getDoctrine()->getManager();

            //Create Home
            $home->setNumber($data["number"]);
            $home->setName($data["placeName"]);
            $home->setHomeAddress($data["homeAddress"]);
            $home->setHomeType($data["homeType"]);

            $em->persist($home);

            //Create Owner
            $owner->setIdentificationNo($data["identificationNo"]);
            $owner->setIdentificationType($data["identificationType"]);
            $owner->setName($data["name"]);
            $owner->setEmail($data["email"]);
            $owner->setTelefonNumber($data["telefonNumber"]);
            $owner->setCurrentAddress($data["currentAddress"]);
            $owner->setReferenceName($data["referenceName"]);
            $owner->setReferenceNumber($data["referenceNumber"]);
            $owner->setReferenceRelationship($data["referenceRelationship"]);

            $em->persist($owner);

            //Create Contract
            $contract->setStartDate($data["startDate"]);
            $contract->setEndDate($data["endDate"]);
            $contract->setRentAmount($data["rentAmount"]);
            $contract->setRentFrequency($data["rentFrequency"]);
            $contract->setRentDeposit($data["rentDeposit"]);
            $contract->setRentUtility($data["rentUtility"]);
            $contract->setContractType("HouseRentAgreement");
            $contract->setRenewal(false);
            $contract->setPlace($home);
            $contract->setPerson($owner);

            $em->persist($contract);

            $em->flush();

             return $this->redirectToRoute('home_show', ['placeId' => $home->getPlaceId()]);
        }

         return $this->render('home/new.html.twig', [
            'home' => $home,
            'home_new' => true,
            'home_link' => true,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{placeId}", name="home_show", methods="GET|POST")
     */
    public function show(Request $request, Home $home): Response
    {
        $contractTable = $this->createDataTable()
            ->add('person', TextColumn::class,['field' => 'p.name','label' => 'Nama Pemilik','globalSearchable' => true])
            ->add('startDate', DateTimeColumn::class,['label' => 'Tarikh Mula','format'=>'d-m-Y','globalSearchable' => true])
            ->add('endDate', DateTimeColumn::class,['label' => 'Tarikh Tamat','format'=>'d-m-Y','globalSearchable' => true])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Contract::class,
                'query' => function (QueryBuilder $builder,$state) use ($home){
                    $builder
                        ->select('c')
                        ->addSelect('p')
                        ->from(Contract::class, 'c')
                        ->leftJoin('c.person', 'p')
                        ->where("c.deletedAt IS NULL")
                        ->andWhere('c.place = :placeId')
                        ->setParameter('placeId',$home->getPlaceId());
                },
                      
            ])
            ->handleRequest($request);

        if($contractTable->isCallBack()){
            return $contractTable->getResponse();
        }



        /*
             <td>{{contract.person.name}}</td>
                <td>{{contract.startDate ? contract.startDate|date('d-m-Y') : '' }}</td>
                <td>{{contract.endDate ? contract.endDate|date('d-m-Y') : '' }}</td>
                <td>MYR{{contract.rentAmount}}</td>
                <td>MYR{{contract.rentDeposit}}</td>
                <td>MYR{{contract.rentUtility}}</td>
                <td>{{ contract.renewal ? 'Ya' : 'Tidak' }}</td>
                <td><a class="btn btn-warning" href="{{ path('home_contractEdit', {'placeId': home.placeId, 'id': contract.id}) }}">Sunting Kontrak</a>
                    {{ include('home/contract/_delete_contract_home_form.html.twig') }}</td>
         */

        return $this->render('home/show.html.twig', [
            'home' => $home,
            'contractTable' => $contractTable
        ]);
    }

    /**
     * @Route("/{placeId}/contract/{id}/generate", name="generateContractHome", methods="GET")
     * @ParamConverter("home", options={"mapping": {"placeId": "placeId"}})
     * @ParamConverter("contract", options={"mapping": {"id": "id"}})
     */
    public function generateContractHome(Request $request,Home $home,Contract $contract){
        /*
            datePrinted
            OwnerName
            ICNo
            RentedHouseAddress
            starDate
            endDate
            rentAmount
            rentDeposit
            rentUtily
         */
        $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);

        $templateProcessor = new TemplateProcessor('resources/perjanjianRumahSewa.docx');
        $templateProcessor->setValue('datePrinted', date('dS F Y'));
        $templateProcessor->setValue('OwnerName', $contract->getPerson()->getName());
        $templateProcessor->setValue('ICNo', $contract->getPerson()->getIdentificationNo());
        $templateProcessor->setValue('RentedHouseAddress',  str_replace("\n","<w:br/>",$home->getHomeAddress()));
         $templateProcessor->setValue('OwnerAddress',  str_replace("\n","<w:br/>",$contract->getPerson()->getCurrentAddress()));
        $templateProcessor->setValue('contractStartDate', $contract->getStartDate()->format('d-m-Y'));
        $templateProcessor->setValue('contractEndDate', $contract->getEndDate()->format('d-m-Y'));
        $startDate = $contract->getStartDate();
        $endDate = $contract->getEndDate();
        $endDate->modify('+1 day');
        $contractInterval = $startDate->diff($endDate);
        $contractIntervalSentence = "";
        if($contractInterval->y > 0){
            $contractIntervalSentence.=" ".$contractInterval->y." YEAR";
        }
        $templateProcessor->setValue('rentPeriod', $contractIntervalSentence);
        $templateProcessor->setValue('rentAmount', $contract->getRentAmount());
        $templateProcessor->setValue('rentAmountRead', strtoupper($f->format($contract->getRentAmount())));
        $templateProcessor->setValue('rentDeposit', $contract->getRentDeposit());
        $templateProcessor->setValue('rentDepositRead', strtoupper($f->format($contract->getRentDeposit())));
        $templateProcessor->setValue('rentUtility', $contract->getRentUtility());
        $templateProcessor->setValue('rentUtilityRead', strtoupper($f->format($contract->getRentUtility())));
        $templateProcessor->saveAs('uploads/perjanjianRumahSewa'.date("dmY").$contract->getId().$home->getPlaceId().$contract->getPerson()->getPersonId().'.docx');

        return $this->file('uploads/perjanjianRumahSewa'.date("dmY").$contract->getId().$home->getPlaceId().$contract->getPerson()->getPersonId().'.docx');
    }



    /**
     * @Route("/{placeId}/contract/new", name="home_contractRenew", methods="GET|POST")
     */
    public function contractRenew(Request $request,Home $home): Response
    {
        $contract = new Contract();

        $form = $this->createForm(RenewHomeContractFormType::class, null);
        $form->handleRequest($request);

        $data = $form->getData();

        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();

            $contract->setStartDate($data["startDate"]);
            $contract->setEndDate($data["endDate"]);
            $contract->setRentAmount($data["rentAmount"]);
            $contract->setRentFrequency($data["rentFrequency"]);
            $contract->setRentDeposit($data["rentDeposit"]);
            $contract->setRentUtility($data["rentUtility"]);
            $contract->setContractType("HouseRentAgreement");
            $contract->setRenewal(true);
            $contract->setPlace($home);
            $contract->setPerson($data["owner"]);

            $em->persist($contract);

            $em->flush();

            return $this->redirectToRoute('home_show', ['placeId' => $home->getPlaceId()]);
        }  


        
        return $this->render('home/contract/renew_contract_form.html.twig', [
            'home' => $home,
            'edit' => false,
            'contract' => null,
            'renewHomeContractForm' => $form->createView()
        ]);
        

    }

    /**
     * @Route("/{placeId}/contract/edit/{id}", name="home_contractEdit", methods="GET|POST")
     * @ParamConverter("home", options={"mapping": {"placeId": "placeId"}})
     * @ParamConverter("contract", options={"mapping": {"id": "id"}})
     */
    public function contractEdit(Request $request,Home $home, Contract $contract): Response
    {

        $form = $this->createForm(RenewHomeContractFormType::class, null);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $data = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $contract->setStartDate($data["startDate"]);
            $contract->setEndDate($data["endDate"]);
            $contract->setRentAmount($data["rentAmount"]);
            $contract->setRentFrequency($data["rentFrequency"]);
            $contract->setRentDeposit($data["rentDeposit"]);
            $contract->setRentUtility($data["rentUtility"]);
            $contract->setContractType("HouseRentAgreement");
            $contract->setRenewal(true);
            $contract->setPlace($home);
            $contract->setPerson($data["owner"]);

            $em->persist($contract);

            $em->flush();

            return $this->redirectToRoute('home_show', ['placeId' => $home->getPlaceId()]);
        }else{
            $newStartDate = $contract->getEndDate();
            $newStartDate->modify("+1 day");
            $form->get("startDate")->setData($contract->getStartDate());
            $form->get("endDate")->setData($newStartDate);
            $form->get("rentAmount")->setData($contract->getRentAmount());
            $form->get("rentFrequency")->setData($contract->getRentFrequency());
            $form->get("rentDeposit")->setData($contract->getRentDeposit());
            $form->get("rentUtility")->setData($contract->getRentUtility());
            $form->get("owner")->setData($contract->getPerson());
            $edit = true;
        }



        
        return $this->render('home/contract/renew_contract_form.html.twig', [
            'home' => $home,
            'contract' => $contract,
            'edit' => $edit,
            'renewHomeContractForm' => $form->createView()
        ]);
        

    }

    /**
     * @Route("/{placeId}/contract/{id}", name="home_contractDelete", methods="DELETE")
     * @ParamConverter("home", options={"mapping": {"placeId": "placeId"}})
     * @ParamConverter("contract", options={"mapping": {"id": "id"}})
     */
    public function contractDelete(Request $request,Home $home,Contract $contract): Response
    {

        if ($this->isCsrfTokenValid('delete'.$contract->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contract);
            $em->flush();
        }

       return $this->redirectToRoute('home_show', ['placeId' => $home->getPlaceId()]);
    }



    /**
     * @Route("/{placeId}/edit", name="home_edit", methods="GET|POST")
     */
    public function edit(Request $request, Home $home): Response
    {
        $form = $this->createForm(HomeType::class, $home);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_edit', ['placeId' => $home->getPlaceId()]);
        }

        return $this->render('home/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{placeId}", name="home_delete", methods="DELETE")
     */
    public function delete(Request $request, Home $home): Response
    {
        if ($this->isCsrfTokenValid('delete'.$home->getPlaceId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($home);
            $em->flush();
        }

        return $this->redirectToRoute('home_index');
    }
}

<?php

namespace App\Controller;

use App\Repository\RoomRepository;
use App\Repository\PersonRepository;
use App\Repository\TenantRepository;
use App\Repository\TransactionRepository;
use App\Repository\ContractRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Controller
{
    /**
     * @Route("/main", name="main")
     */
    public function index(ContractRepository $contractRepository, TransactionRepository $transactionRepository, RoomRepository $roomRepository,PersonRepository $personRepository): Response
    {	

    	$transactionsPending = $transactionRepository->findBy(
    		[
    			'status'  => 'PENDING',
    		],
    		['dateIssued' => 'DESC']
    	);


        $rooms = $roomRepository->findAll();

        $contractsAboutToExpire = $contractRepository->findAboutToExpire();
        $contractsExpired = $contractRepository->findExpired();
        $contractsNotRenewed = $contractRepository->findNotRenewing();
        $people = $personRepository->findAll();

        return $this->render('main/index.html.twig',[
        	'transactionsPending' => $transactionsPending,
            'contractsAboutToExpire' => $contractsAboutToExpire,
            'contractsExpired' => $contractsExpired,
            'contractsNotRenewed' => $contractsNotRenewed,
            'people' => $people,
            'rooms' => $rooms,
            'main' => true
        ]);
    }

}

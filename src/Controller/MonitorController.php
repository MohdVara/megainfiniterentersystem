<?php

namespace App\Controller;

use App\Repository\HomeRepository;
use App\Repository\OwnerRepository;
use App\Repository\TransactionRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/monitor")
 */
class MonitorController extends Controller
{
    /**
     * @Route("/monthlyRentTracker", name="monthlyRentTracker")
     */
    public function monthlyRentTracker(HomeRepository $homeRepository, TransactionRepository $transactionRepository)
    {	
    	$transactionsPending = $transactionRepository->findBy(
    		['status'  => 'PENDING'],
    		['dateIssued' => 'DESC']
    	);
    	$homes = $homeRepository->findAll();

        return $this->render('monitor/monthlyRentTracker.html.twig', [
            'transactionsPending' => $transactionsPending,
            'homes' => $homes,
            'tracker' => true,
            "monthlyRentTracker" => true
        ]);
    }

    /**
     * @Route("/tenantContractTracker", name="tenantContractTracker")
     */
    public function tenantContractTracker(HomeRepository $homeRepository)
    {	
    	$homes = $homeRepository->findAll();
        return $this->render('monitor/tenantContractTracker.html.twig', [
            'homes' => $homes,
            'tracker' => true,
            "tenantContractTracker" => true
        ]);
    }

    /**
     * @Route("/monitor", name="ownerContractTracker")
     */
    public function ownerContractTracker(HomeRepository $homeRepository)
    {
    	$homes = $homeRepository->findAll();
        return $this->render('monitor/ownerContractTracker.html.twig', [
            'homes' => $homes,
            'tracker' => true,
            "ownerContractTracker" => true
        ]);
    }
}

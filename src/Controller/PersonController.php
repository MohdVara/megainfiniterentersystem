<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Owner;
use App\Entity\Tenant;
use App\Entity\Contract;
use App\Entity\Room;
use App\Entity\User;
use App\Form\PersonType;
use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Omines\DataTablesBundle\Adapter\ArrayAdapter;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\Controller\DataTablesTrait;
use Doctrine\ORM\QueryBuilder;


class PersonController extends Controller
{
    use DataTablesTrait;

    /**
     * @Route("/listPeople", name="list_people", methods="GET|POST")
     */
    public function listPeople(Request $request){
        $em =  $this->getDoctrine()->getManager();
        $personRepository = $em->getRepository("App\Entity\Person");
        $peopleList = $personRepository->createQueryBuilder('p')
                        ->select(['p.personId as id',
                                  "CONCAT('( ',p.identificationNo,' )', p.name) AS text"])->getQuery()->getResult();

        return new JsonResponse($peopleList);
    }   

    /**
     * @Route("/tenant", name="person_index_tenant", methods="GET|POST")
     */
    public function tenant_index(Request $request,PersonRepository $personRepository): Response
    {   
        $tenantTable = $this->createDataTable()
            ->add('personId',TextColumn::class,['visible' => false ])
            ->add('identificationNo', TextColumn::class,['label' => 'No. Pengenalan','globalSearchable' => true])
            ->add('name', TextColumn::class,['label' => 'Nama','globalSearchable' => true])
            ->add('telefonNumber', TextColumn::class,['label' => 'No. Telefon','globalSearchable' => true])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Tenant::class,
                'query' => function (QueryBuilder $builder){
                    $builder
                        ->select('t')
                        ->from(Tenant::class, 't')
                        ->where("t.deletedAt IS NULL");
                },
                      
            ])
            ->handleRequest($request);

        if($tenantTable->isCallBack()){
            return $tenantTable->getResponse();
        }

        return $this->render('person/index.html.twig', [
            'tableTitle' => "Penyewa Aktif",
            'person_link' => true,
            'tenantIndex' => true,
            'table' => $tenantTable
        ]);
    }

    /**
     * @Route("/past_tenant", name="person_index_past_tenant", methods="GET|POST")
     */
    public function past_tenant_index(Request $request,PersonRepository $personRepository): Response
    {   
        $tenantTable = $this->createDataTable()
            ->add('personId',TextColumn::class,['visible' => false ])
            ->add('identificationNo', TextColumn::class,['label' => 'No. Pengenalan','globalSearchable' => true])
            ->add('name', TextColumn::class,['label' => 'Nama','globalSearchable' => true])
            ->add('telefonNumber', TextColumn::class,['label' => 'No. Telefon','globalSearchable' => true])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Tenant::class,
                'query' => function (QueryBuilder $builder){
                    $builder
                        ->select('t')
                        ->from(Tenant::class, 't')
                        ->where("t.deletedAt IS NOT NULL");
                },
                      
            ])
            ->handleRequest($request);

        if($tenantTable->isCallBack()){
            return $tenantTable->getResponse();
        }

        return $this->render('person/index.html.twig', [
            'tableTitle' => "Penyewa Tidak Aktif",
            'person_link' => true,
            'tenantIndex' => true,
            'table' => $tenantTable
        ]);
    }

    /**
     * @Route("/owner", name="person_index_owner", methods="GET|POST")
     */
    public function owner_index(Request $request,PersonRepository $personRepository): Response
    {   
        $person = $personRepository->findAll()[0];
        $ownerTable = $this->createDataTable()
            ->add('personId',TextColumn::class,['visible' => false ])
            ->add('identificationNo', TextColumn::class,['label' => 'No. Pengenalan','globalSearchable' => true])
            ->add('name', TextColumn::class,['label' => 'Nama','globalSearchable' => true])
            ->add('telefonNumber', TextColumn::class,['label' => 'No. Telefon','globalSearchable' => true])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Owner::class,
                'query' => function (QueryBuilder $builder){
                    $builder
                        ->select('o')
                        ->addSelect('o')
                        ->from(Owner::class, 'o')
                        ->where("o.deletedAt IS NULL");
                },
                      
            ])
            ->handleRequest($request);

        if($ownerTable->isCallBack()){
            return $ownerTable->getResponse();
        }


        return $this->render('person/index.html.twig', [
            'people' => $personRepository->findAll(),
            'tableTitle' => "Pemilik",
            'person_link' => true,
            'ownerIndex' => true,
            'table' => $ownerTable
        ]);
    }

    /**
     * @Route("/", name="person_index", methods="GET")
     */
    /*
    public function index(PersonRepository $personRepository): Response
    {   
        $person = $personRepository->findAll()[0];

        return $this->render('person/index.html.twig', [
            'people' => $personRepository->findAll()
        ]);
    }
    */

    /**
     * @Route("person/new", name="person_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($person);
            $em->flush();

            return $this->redirectToRoute('person_index');
        }

        return $this->render('person/new.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
            'person_link' => true,
            'person_index' => true
        ]);
    }

    /**
     * @Route("person/{personId}", name="person_show", methods="GET|POST")
     */
    public function show(Request $request,Person $person): Response
    {
        $title = "Kenalan";
        switch($person->getClass()){
            case "App\Entity\Tenant":
                    $title = "Penyewa";
                    $header = "No. Bilik";
                break;

            case "App\Entity\Owner":
                    $title = "Pemilik";
                    $header = "No. Rumah";
                break;
            default:
                break;
        }

         $contractTable = $this->createDataTable()
             ->add('contractId', TextColumn::class,['field' => 'c.id', 'visible' => false])
            ->add('roomNumber', TextColumn::class,['field' => 'cr.number','label' => $header ,'globalSearchable' => true])
            ->add('startDate', DateTimeColumn::class,['label' => 'Tarikh Mula','format'=>'d-m-Y','globalSearchable' => true])
            ->add('endDate', DateTimeColumn::class,['label' => 'Tarikh Tamat','format'=>'d-m-Y','globalSearchable' => true])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Contract::class,
                'query' => function (QueryBuilder $builder,$state) use ($person){
                    $builder
                        ->select('c')
                        ->addSelect('cp')
                        ->from(Contract::class, 'c')
                        ->leftJoin('c.person', 'cp')                               
                        ->leftJoin('c.place','cr')
                        ->where("c.deletedAt IS NULL")
                        ->andWhere('cp.personId = :personId')
                        ->setParameter('personId',$person->getPersonId());
                },
                      
            ])
            ->handleRequest($request);

        if($contractTable->isCallBack()){
            return $contractTable->getResponse();
        }


        return $this->render('person/show.html.twig', [
            'person' => $person,
            'title' => $title,
            'contractTable' => $contractTable,
            'person_link' => true
        ]);
    }

    /**
     * @Route("person/{personId}/edit", name="person_edit", methods="GET|POST")
     */
    public function edit(Request $request, Person $person): Response
    {
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('person_edit', ['personId' => $person->getPersonId()]);
        }

        return $this->render('person/edit.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
            'person_link' => true
        ]);
    }

    /**
     * @Route("person/{personId}", name="person_delete", methods="DELETE")
     */
    public function delete(Request $request, Person $person): Response
    {
        switch ($person->getClass()) {
            case "App\Entity\Tenant":
                $personType = "Tenant";
                break;
            case "App\Entity\Owner":
                $personType = "Owner";
                break;
            default:
            break;
        }

        if ($this->isCsrfTokenValid('delete'.$person->getPersonId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($person);
            $em->flush();
        }

        switch ($personType) {
            case "Tenant":
                    return $this->redirectToRoute('person_index_tenant');
                break;
            case "Owner":
                    return $this->redirectToRoute('person_index_owner');
                break;
            default:
            break;
        }

        
    }
}

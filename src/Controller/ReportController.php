<?php

namespace App\Controller;

use App\Repository\TransactionRepository;
use App\Repository\HomeRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ReportController extends Controller
{
    /**
     * @Route("/report", name="report")
     */
    public function index()
    {

        return $this->render('report/index.html.twig', [

        ]);
    }

    /**
     * @Route("/sales/monthly/{month}/{year}", name="montlySalesReport", methods="GET|POST")
     */
    public function montlySalesReport(HomeRepository $homeRepository,$month,$year)
    {	
    	$homes = $homeRepository->findAll();

        return $this->render('report/montlySalesReport.html.twig', [
            'report' => true,
        	'monthlySalesReport' => true,
            'homes' => $homes,
        	'month' => $month,
        	'year' => $year
        ]);
    }

    /**
     * @Route("/sales/yearly/{year}", name="yearlySalesReport", methods="GET|POST")
     */
    public function yearlySalesReport(HomeRepository $homeRepository,$year)
    {

    	$homes = $homeRepository->findAll();
        $yearlyReport = array();

        return $this->render('report/yearlySalesReport.html.twig', [
            'report' => true,
            'yearlySalesReport' => true,
        	'homes' => $homes,
        	'year' => $year
        ]);
    }
}

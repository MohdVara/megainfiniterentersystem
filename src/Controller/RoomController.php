<?php

namespace App\Controller;

use App\Entity\Tenant;
use App\Entity\Room;
use App\Entity\Home;
use App\Entity\Contract;
use App\Entity\Transaction;
use App\Form\RoomType;
use App\Form\RoomWithHomeType;
use App\Form\NewTenantFormType;
use App\Form\RenewRoomContractFormType;
use App\Repository\ContractRepository;
use App\Repository\RoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use PhpOffice\PhpWord\TemplateProcessor;
use Omines\DataTablesBundle\Adapter\ArrayAdapter;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\Controller\DataTablesTrait;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use PDOException;
use Exception;

use Doctrine\ORM\QueryBuilder;

/**
 * @Route("/room")
 */
class RoomController extends Controller
{
     use DataTablesTrait;
    /**
     * @Route("/", name="room_index", methods="GET")
     */
    public function index(RoomRepository $roomRepository): Response
    {
        //return new JsonResponse($roomRepository->selectAllRoomWithCountTenants());
        return $this->render('room/index.html.twig', [
            'rooms' => $roomRepository->findAll(),
            'room_link' => true,
            "room_index" => true 
        ]);
    }

    /**
     * @Route("/new", name="room_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $room = new Room();
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($room);
            $em->flush();

            return $this->redirectToRoute('home_show', ['placeId' => $room->getHome()->getPlaceId()]);
        }

        return $this->render('room/new.html.twig', [
            'room' => $room,
            'form' => $form->createView(),
            'room_link' => true,
            "room_new" => true 
        ]);
    }

    /**
     * @Route("/new/at/{placeId}", name="room_new_with_home", methods="GET|POST")
     * @ParamConverter("home", options={"mapping": {"placeId": "placeId"}})
     */
    public function new_by_home(Request $request,Home $home): Response
    {
        $room = new Room();
        $form = $this->createForm(RoomWithHomeType::class, null);
        $form->handleRequest($request);

        $data = $form->getData();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $room->setNumber($data["number"]);
            $room->setRoomType($data["roomType"]);
            $room->setCapacity($data["capacity"]);
            $room->setHome($home);


            $em->persist($room);
            $em->flush();

            return $this->redirectToRoute('home_show', ['placeId' => $home->getPlaceId()]);
        }

        return $this->render('room/new.html.twig', [
            'room' => $room,
            'home' => $home,
            'form' => $form->createView(),
            'room_link' => true,
            "room_new" => true 
        ]);
    }


    /**
     * @Route("/{placeId}", name="room_show", methods="GET|POST")
     */
    public function show(Request $request,ContractRepository $contractRepository,Room $room): Response
    {
        //$contracts = $contractRepository->findMostRecentTenantContractByRoom($room->getPlaceId());
        $contracts = $contractRepository->findBy(
            ['place' => $room->getPlaceId() ]
        );

         $contractTable = $this->createDataTable()
            ->add('person', TextColumn::class,['field' => 'p.name','label' => 'Nama Pemilik','globalSearchable' => true])
            ->add('startDate', DateTimeColumn::class,['label' => 'Tarikh Mula','format'=>'d-m-Y','globalSearchable' => true])
            ->add('endDate', DateTimeColumn::class,['label' => 'Tarikh Tamat','format'=>'d-m-Y','globalSearchable' => true])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Contract::class,
                'query' => function (QueryBuilder $builder,$state) use ($room){
                    $builder
                        ->select('c')
                        ->addSelect('p')
                        ->from(Contract::class, 'c')
                        ->leftJoin('c.person', 'p')
                        ->where("c.deletedAt IS NULL")
                        ->andWhere('c.place = :placeId')
                        ->andWhere(':now BETWEEN c.startDate AND c.endDate')
                        ->setParameter('placeId',$room->getPlaceId())
                        ->setParameter('now',new \DateTime("now"));
                },
                      
            ])
            ->handleRequest($request);

        if($contractTable->isCallBack()){
            return $contractTable->getResponse();
        }

        

        return $this->render('room/show.html.twig', [
            'room' => $room,
            'contracts' => $contracts,
            'contractTable' => $contractTable
        ]);
    }

    /**
     * @Route("/{placeId}/edit", name="room_edit", methods="GET|POST")
     */
    public function edit_by_home(Request $request, Room $room): Response
    {
        $form = $this->createForm(RoomWithHomeType::class,null);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();



            return $this->redirectToRoute('home_show', ['placeId' => $home->getPlaceId()]);
        }


        return $this->render('room/edit.html.twig', [
            'room' => $room,
            'form' => $form->createView(),
            'room_link' => true,
            "room_edit" => true 
        ]);
    }

     /**
     * @Route("/{placeId}/edit/by/home", name="room_edit_by_home", methods="GET|POST")
     */
    public function edit(Request $request, Room $room): Response
    {
        $form = $this->createForm(RoomWithHomeType::class, null);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $room->setNumber($data["number"]);
            $room->setRoomType($data["roomType"]);
            $room->setCapacity($data["capacity"]);
            $room->setHome($home);
            $em->persist($room);
            $em->flush();

            return $this->redirectToRoute('home_show', ['placeId' => $home->getPlaceId()]);
        } 

        $form->get("number")->setData($room->getNumber());
        $form->get("roomType")->setData($room->getRoomType());
        $form->get("capacity")->setData($room->getCapacity());

        return $this->render('room/edit.html.twig', [
            'room' => $room,
            'form' => $form->createView(),
            'room_link' => true,
            "room_edit" => true 
        ]);
    }

    /**
     * @Route("/{placeId}", name="room_delete", methods="DELETE")
     */
    public function delete(Request $request, Room $room): Response
    {
        if ($this->isCsrfTokenValid('delete'.$room->getPlaceId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($room);
            $em->flush();
        }

        return $this->redirectToRoute('home_show', ['placeId' => $room->getHome()->getPlaceId()]);
        
    }


    /**
     * @Route("/tenant/new", name="room_tenantNew", methods="GET|POST")
     */
    public function tenantNew(Request $request): Response
    {
        $tenant = new Tenant();
        $contract = new Contract();

        $form = $this->createForm(NewTenantFormType::class, null);
        $form->handleRequest($request);

        $data = $form->getData();

        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();

            //Create Tenant
            $tenant->setIdentificationNo($data["identificationNo"]);
            $tenant->setIdentificationType($data["identificationType"]);
            $tenant->setName($data["name"]);
            $tenant->setEmail($data["email"]);
            $tenant->setTelefonNumber($data["telefonNumber"]);
            $tenant->setCurrentAddress($data["currentAddress"]);
            $tenant->setReferenceName($data["referenceName"]);
            $tenant->setReferenceNumber($data["referenceNumber"]);
            $tenant->setReferenceRelationship($data["referenceRelationship"]);

            $contract->setStartDate($data["startDate"]);
            $contract->setEndDate($data["endDate"]);
            $contract->setRentAmount($data["rentAmount"]);
            $contract->setRentFrequency($data["rentFrequency"]);
            $contract->setRentDeposit($data["rentDeposit"]);
            $contract->setRentUtility($data["rentUtility"]);
            $contract->setContractType("RoomRentAgreement");
            $contract->setRenewal(false);
            $contract->setPlace($data["room"]);
            $contract->setPerson($tenant);

            //First Period Rental
           if($contract->getStartDate() <= date_modify(new \Datetime('now'),"+60 days")
                && $contract->getStartDate() >= date_modify(new \Datetime('now'),"-30 days")){    
            
            $currentMonthRentTransaction = new Transaction();
            if($data["rentFrequency"] == "Monthly"){
                $currentMonthRentTransaction->setTransactionDescription('Sewa Bulan '.date("m-Y"));
            }elseif($data["rentFrequency"] == "Weekly"){
                $currentMonthRentTransaction->setTransactionDescription('Sewa Minggu '.date("d-m-Y"));
            }else{
                $currentMonthRentTransaction->setTransactionDescription('Sewa Tahun '.date("Y"));
            }
                $currentMonthRentTransaction->setTransactionAmount($data["rentAmount"]);
                $currentMonthRentTransaction->setTransactionType("DEBIT");
                $currentMonthRentTransaction->setDateIssued(new \DateTime('now'));
                $currentMonthRentTransaction->setStatus("PENDING");
                $currentMonthRentTransaction->setPerson($tenant);

                $em->persist($currentMonthRentTransaction);

            //New  Registration Current Month
                $rentDepositTransaction = new Transaction();
                $rentDepositTransaction->setTransactionDescription("Deposit Sewa");
                $rentDepositTransaction->setTransactionAmount($data["rentDeposit"]);
                $rentDepositTransaction->setTransactionType("DEBIT");
                $rentDepositTransaction->setDateIssued(new \DateTime('now'));
                $rentDepositTransaction->setStatus("PENDING");
                $rentDepositTransaction->setPerson($tenant);

                $em->persist($rentDepositTransaction);

                $utiliyDepositTransaction = new Transaction();
                $utiliyDepositTransaction->setTransactionDescription("Utility Deposit Sewa");
                $utiliyDepositTransaction->setTransactionAmount($data["rentUtility"]);
                $utiliyDepositTransaction->setTransactionType("DEBIT");
                $utiliyDepositTransaction->setDateIssued(new \DateTime('now'));
                $utiliyDepositTransaction->setStatus("PENDING");
                $utiliyDepositTransaction->setPerson($tenant); 

                $em->persist($utiliyDepositTransaction);
            }

            //Other Payments
            if($data["totalOtherPayments"] 
                && $data["otherPaymentsDetails"]){
                //Other payments
                $totalOtherPaymentsTransaction = new Transaction();
                $totalOtherPaymentsTransaction->setTransactionDescription($data["otherPaymentsDetails"]);
                $totalOtherPaymentsTransaction->setTransactionAmount($data["totalOtherPayments"]);
                $totalOtherPaymentsTransaction->setTransactionType("DEBIT");
                $totalOtherPaymentsTransaction->setDateIssued(new \DateTime('now'));
                $totalOtherPaymentsTransaction->setStatus("PENDING");
                $totalOtherPaymentsTransaction->setPerson($tenant);

                $em->persist($totalOtherPaymentsTransaction); 

            }elseif($data["totalOtherPayments"]){
                 $message = sprintf('Butiran lain-lain bayaran perlu diisi. ');
                 array_push($errors,$message);
            }elseif($data["otherPaymentsDetails"]){
                $message = sprintf('Jumlah lain-lain bayaran perlu diisi. ');
                array_push($errors,$message);
            }
            

            try{
                $em->flush();
            } catch (DBALException $e) {
                    $message = sprintf('DBALException [%i]: %s', $e->getCode(), $e->getMessage());
            } catch (PDOException $e) {
                    $message = sprintf('PDOException [%i]: %s', $e->getCode(), $e->getMessage());
            } catch (ORMException $e) {
                    $message = sprintf('ORMException [%i]: %s', $e->getCode(), $e->getMessage());
            } catch (Exception $e) {
                    $message = sprintf('Exception [%i]: %s', $e->getCode(), $e->getMessage());
            }

            array_push($errors,$message);

            if($errors){
                $this->get('session')->getFlashBag()->set('errors',$errors);
                $form->get("identificationNo")->setData($data["identificationNo"]);
                $form->get("identificationType")->setData($data["identificationType"]);
                $form->get("name")->setData($data["name"]);
                $form->get("email")->setData($data["email"]);
                $form->get("telefonNumber")->setData($data["telefonNumber"]);
                $form->get("currentAddress")->setData($data["currentAddress"]);
                $form->get("referenceName")->setData($data["referenceName"]);
                $form->get("referenceNumber")->setData($data["referenceNumber"]);
                $form->get("referenceRelationship")->setData($data["referenceRelationship"]);
                $form->get("startDate")->setData($data["startDate"]);
                $form->get("endDate")->setData($data["endDate"]);
                $form->get("rentAmount")->setData($data["rentAmount"]);
                $form->get("rentFrequency")->setData($data["rentFrequency"]);
                $form->get("rentDeposit")->setData($data["rentDeposit"]);
                $form->get("rentUtility")->setData($data["rentUtility"]);
                $form->get("totalOtherPayments")->setData($data["totalOtherPayments"]);
                $form->get("otherPaymentsDetails")->setData($data["otherPaymentsDetails"]);

                return $this->redirectToRoute("room_tenantNew");
            }

             $authenticated_user = $this->getUser();

             if(in_array("ROLE_SALES",$authenticated_user->getRoles())){
                return $this->redirectToRoute("room_tenantNew");
             }
             else{
                return $this->redirectToRoute('room_show', ['placeId' => $data["room"]->getPlaceId()]);
             }

            
        }



        
        return $this->render('room/new_tenant.html.twig', [
            'renewRoomContractForm' => $form->createView(),
            'room_link' => true,
            "room_tenant_new" => true 
        ]);
        

    }

    /**
     * @Route("/{placeId}/contract/{id}/generate", name="generateContractRoom", methods="GET")
     * @ParamConverter("room", options={"mapping": {"placeId": "placeId"}})
     * @ParamConverter("contract", options={"mapping": {"id": "id"}})
     */
    public function generateContractRoom(Request $request,Room $room,Contract $contract){
        $templateProcessor = new TemplateProcessor('resources/perjanjianBilikSewa.docx');
        $templateProcessor->setValue('namaPenyewa', $contract->getPerson()->getName());
        $templateProcessor->setValue('alamatPenyewa', str_replace("\n","<w:br/>",$contract->getPerson()->getCurrentAddress()));
        $templateProcessor->setValue('idPenyewa', $contract->getPerson()->getIdentificationNo());
        $templateProcessor->setValue('telefonPenyewa', $contract->getPerson()->getTelefonNumber());

        //Check duration between dates
        $startDate = $contract->getStartDate();
        $endDate = $contract->getEndDate();
        $endDate->modify('+1 day');
        $contractInterval = $startDate->diff($endDate);
        $endDate->modify('-1 day');
        $contractIntervalSentence = "";
        if($contractInterval->y > 0){
            $contractIntervalSentence.=" ".$contractInterval->y." TAHUN";
        }
        if($contractInterval->m > 0){
            $contractIntervalSentence.=" ".$contractInterval->m." BULAN";
        }
        if($contractInterval->d > 0){
            $contractIntervalSentence.=" ".$contractInterval->d." HARI";
        }
        $templateProcessor->setValue('jangkaMasaKontrak', $contractIntervalSentence);
        $templateProcessor->setValue('tarikhMulaKontrak', $startDate->format('d-m-Y'));
        $templateProcessor->setValue('tarikhAkhirKontrak', $endDate->format('d-m-Y'));
        $templateProcessor->setValue('alamatRumahSewa', str_replace("\n","<w:br/>", $room->getHome()->getHomeAddress()));
        $templateProcessor->setValue('sewaBulanan', $contract->getRentAmount());
        $templateProcessor->setValue('depositSewaRumah', $contract->getRentDeposit());
        $templateProcessor->setValue('depositKemudahanAsas', $contract->getRentUtility());
        $templateProcessor->setValue('namaSaksi', $this->getUser()->getName());
        $templateProcessor->setValue('idSaksi', $this->getUser()->getIdentificationNo());
        $templateProcessor->setValue('namaRujukan',$contract->getPerson()->getReferenceName());
        $templateProcessor->setValue('noTelefonRujukan',$contract->getPerson()->getReferenceNumber());
        $templateProcessor->saveAs('uploads/perjanjianBilikSewa'.date("dmY").$contract->getId().$room->getPlaceId().$contract->getPerson()->getPersonId().'.docx');

        return $this->file('uploads/perjanjianBilikSewa'.date("dmY").$contract->getId().$room->getPlaceId().$contract->getPerson()->getPersonId().'.docx');
    }



    /**
     * @Route("/{placeId}/contract/{id}/new", name="room_contractRenew", methods="GET|POST")
     * @ParamConverter("room", options={"mapping": {"placeId": "placeId"}})
     * @ParamConverter("contract", options={"mapping": {"id": "id"}})
     */
    public function contractRenew(Request $request,Room $room, Contract $old_contract): Response
    {
        $contract = new Contract();

        $form = $this->createForm(RenewRoomContractFormType::class, null);
        $form->handleRequest($request);

        $data = $form->getData();

        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();

            $contract->setStartDate($data["startDate"]);
            $contract->setEndDate($data["endDate"]);
            $contract->setRentAmount($data["rentAmount"]);
            $contract->setRentFrequency($data["rentFrequency"]);
            $contract->setRentDeposit($data["rentDeposit"]);
            $contract->setRentUtility($data["rentUtility"]);
            $contract->setContractType("RoomRentAgreement");
            $contract->setRenewal(true);
            $contract->setPlace($room);
            $contract->setPerson($old_contract->getPerson());

            $em->persist($contract);

            $em->flush();

            //return $this->redirectToRoute('room_show', ['placeId' => $room->getPlaceId()]);
            return $this->redirectToRoute('contract_show', ['id' => $contract->getId()]);
            
        }

        $newStartDate = clone $old_contract->getEndDate();
        $newStartDate->modify("+1 day");
        $form->get("startDate")->setData($newStartDate);
        $form->get("rentAmount")->setData($old_contract->getRentAmount());
        $form->get("rentFrequency")->setData($old_contract->getRentFrequency());
        $form->get("rentDeposit")->setData($old_contract->getRentDeposit());
        $form->get("rentUtility")->setData($old_contract->getRentUtility());
        
        return $this->render('room/contract/renew_contract_form.html.twig', [
            'room' => $room,
            'contract' => $old_contract,
            'edit' => false,
            'renewRoomContractForm' => $form->createView(),
            'room_link' => true, 
            "room_show" => true 
        ]);
        

    }

    /**
     * @Route("/{placeId}/contract/edit/{id}", name="room_contractEdit", methods="GET|POST")
     * @ParamConverter("room", options={"mapping": {"placeId": "placeId"}})
     * @ParamConverter("contract", options={"mapping": {"id": "id"}})
     */
    public function contractEdit(Request $request,Room $room, Contract $old_contract): Response
    {


        $form = $this->createForm(RenewRoomContractFormType::class, null);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $contract = $old_contract;

            $contract->setStartDate($data["startDate"]);
            $contract->setEndDate($data["endDate"]);
            $contract->setRentAmount($data["rentAmount"]);
            $contract->setRentFrequency($data["rentFrequency"]);
            $contract->setRentDeposit($data["rentDeposit"]);
            $contract->setRentUtility($data["rentUtility"]);
            $contract->setContractType("RoomRentAgreement");
            $contract->setRenewal($old_contract->getRenewal());
            $contract->setPlace($room);
            $contract->setPerson($old_contract->getPerson());

            $em->persist($contract);

            $em->flush();

            return $this->redirectToRoute('room_show', ['placeId' => $room->getPlaceId()]);
        }else{
            $form->get("startDate")->setData($old_contract->getStartDate());
            $form->get("endDate")->setData($old_contract->getEndDate());
            $form->get("rentAmount")->setData($old_contract->getRentAmount());
            $form->get("rentFrequency")->setData($old_contract->getRentFrequency());
            $form->get("rentDeposit")->setData($old_contract->getRentDeposit());
            $form->get("rentUtility")->setData($old_contract->getRentUtility());
            $edit = true;
        }



        
        return $this->render('room/contract/renew_contract_form.html.twig', [
            'room' => $room,
            'contract' => $old_contract,
            'edit' => $edit,
            'renewRoomContractForm' => $form->createView(),
            'room_link' => true,
            "room_show" => true 
        ]);
        

    }

    /**
     * @Route("/{placeId}/contract/{id}", name="room_contractDelete", methods="DELETE")
     * @ParamConverter("room", options={"mapping": {"placeId": "placeId"}})
     * @ParamConverter("contract", options={"mapping": {"id": "id"}})
     */
    public function contractDelete(Request $request,Room $room,Contract $contract): Response
    {

        if ($this->isCsrfTokenValid('delete'.$contract->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contract);
            $em->flush();
        }

       return $this->redirectToRoute('room_show', ['placeId' => $room->getPlaceId()]);
    }
}

<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{

    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {
    	// get the login error if there is one
    	$error = $authenticationUtils->getLastAuthenticationError();

    	// last username entered by the user
    	$lastUsername = $authenticationUtils->getLastUsername();

    	return $this->render('security/login.html.twig', array(
        	'last_username' => $lastUsername,
        	'error'         => $error,
    	));
    }

    /**
     * @Route("/login_success", name="login_success")
     */
    public function postLoginRedirectAction()
    {   
         $user = $this->getUser();

        if(in_array("ROLE_MANAGER",$user->getRoles())) {
            return $this->redirectToRoute("main");
        } else if(in_array("ROLE_ADMIN",$user->getRoles())){
            return $this->redirectToRoute("main");
        } else if(in_array("ROLE_SALES",$user->getRoles())){
            return $this->redirectToRoute("room_tenantNew");
        }
    }
}

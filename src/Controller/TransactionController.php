<?php

namespace App\Controller;

use App\Entity\Transaction;
use App\Form\TransactionType;
use App\Repository\TransactionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Omines\DataTablesBundle\Adapter\ArrayAdapter;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\Controller\DataTablesTrait;
use Doctrine\ORM\QueryBuilder;

/**
 * @Route("/transaction")
 */
class TransactionController extends Controller
{
     use DataTablesTrait;
    /**
     * @Route("/", name="transaction_index", methods="GET|POST")
     */
    public function index(Request $request,TransactionRepository $transactionRepository): Response
    {
         $transactionTable = $this->createDataTable()
            ->add('transactionId', TextColumn::class,['field' => 't.id','visible' => false])
            ->add('transactionDescription', TextColumn::class,['label' => 'Butiran Pembayaran','globalSearchable' => true])
            ->add('transactionAmount', TextColumn::class,['label' => 'Amaun','globalSearchable' => true])
            ->add('transactionType', TextColumn::class,['label' => 'Jenis Pembayaran','globalSearchable' => true])
            ->add('status', TextColumn::class,['label' => 'Status','globalSearchable' => true])
            ->add('dateIssued', DateTimeColumn::class,['label' => 'Tarikh DiKeluarkan','format'=>'d-m-Y','globalSearchable' => true])
            ->add('datePaid', DateTimeColumn::class,['label' => 'Tarikh Dibayar','format'=>'d-m-Y','globalSearchable' => true])
            ->add('person', TextColumn::class,['field' => 'p.name','label' => 'Pembayar','globalSearchable' => true])
            ->createAdapter(ORMAdapter::class, [
                'entity' => Transaction::class,
                'query' => function (QueryBuilder $builder,$state){
                    $builder
                        ->select('t')
                        ->addSelect('p')
                        ->from(Transaction::class, 't')
                        ->leftJoin('t.person', 'p')
                        ->where("t.deletedAt IS NULL");
                },
                      
            ])
            ->handleRequest($request);

        if($transactionTable->isCallBack()){
            return $transactionTable->getResponse();
        }

        return $this->render('transaction/index.html.twig', [
            'transactions' => $transactionRepository->findAll(),
            'transactionTable' => $transactionTable,
            "transaction_link" => true, 
            "transaction_index" => true 
        ]);
    }

    /**
     * @Route("/new", name="transaction_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $transaction = new Transaction();
        $form = $this->createForm(TransactionType::class, $transaction);
        $form->handleRequest($request);

       if ($form->isSubmitted() && $form->isValid()) {
            if($transaction->getDatePaid() != null){
                 $transaction->setStatus("PAID");
                 $em->persist($transaction);
            }else{
                 $transaction->setStatus("PENDING");
                 $em->persist($transaction);
            }
            $em->flush();

            return $this->redirectToRoute('transaction_index');
            //return $this->redirectToRoute('transaction_edit', ['id' => $transaction->getId()]);
        }

        return $this->render('transaction/new.html.twig', [
            'transaction' => $transaction,
            'form' => $form->createView(),
            "transaction_link" => true, 
            "transaction_new" => true 
        ]);
    }

    /**
     * @Route("/{id}", name="transaction_show", methods="GET")
     */
    public function show(Transaction $transaction): Response
    {
        return $this->render('transaction/show.html.twig', [
            'transaction' => $transaction,
            "transaction_link" => true, 
            "transaction_show" => true 
        ]);
    }

     /**
     * @Route("paid/{id}", name="transaction_paid", methods="GET")
     */
    public function paid(Transaction $transaction): Response
    {
        $em = $this->getDoctrine()->getManager();
        $transaction->setStatus("PAID");
        $transaction->setDatePaid(new \DateTime());
        $em->persist($transaction);
        $em->flush();

        return $this->redirectToRoute('transaction_index');

    }

    /**
     * @Route("/{id}/edit", name="transaction_edit", methods="GET|POST")
     */
    public function edit(Request $request, Transaction $transaction): Response
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(TransactionType::class, $transaction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($transaction->getDatePaid() != null){
                 $transaction->setStatus("PAID");
                 $em->persist($transaction);
            }else{
                 $transaction->setStatus("PENDING");
                 $em->persist($transaction);
            }
            $em->flush();

            return $this->redirectToRoute('transaction_index');
            //return $this->redirectToRoute('transaction_edit', ['id' => $transaction->getId()]);
        }

        return $this->render('transaction/edit.html.twig', [
            'transaction' => $transaction,
            'form' => $form->createView(),
            "transaction_link" => true, 
            "transaction_edit" => true 
        ]);
    }

    /**
     * @Route("/{id}", name="transaction_delete", methods="DELETE")
     */
    public function delete(Request $request, Transaction $transaction): Response
    {
        if ($this->isCsrfTokenValid('delete'.$transaction->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($transaction);
            $em->flush();
        }

        return $this->redirectToRoute('transaction_index');
    }
}

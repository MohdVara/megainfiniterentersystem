<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Omines\DataTablesBundle\Adapter\ArrayAdapter;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Controller\DataTablesTrait;
use Doctrine\ORM\QueryBuilder;

/**
 * @Route("/user")
 */
class UserController extends Controller
{
     use DataTablesTrait;
    /**
     * @Route("/", name="user_index", methods="GET|POST")
     */
    public function index(Request $request,UserRepository $userRepository): Response
    {
        $userTable = $this->createDataTable()
            ->add('personId', TextColumn::class,['visible' => false])
            ->add('identificationNo', TextColumn::class,['label' => 'No. Pengenalan','globalSearchable' => true])
            ->add('name', TextColumn::class,['label' => 'Nama','globalSearchable' => true])
            ->add('telefonNumber', TextColumn::class,['label' => 'No. Telefon','globalSearchable' => true])
            ->createAdapter(ORMAdapter::class, [
                'entity' => User::class,
                'query' => function (QueryBuilder $builder){
                    $builder
                        ->select('u')
                        ->addSelect('u')
                        ->from(User::class, 'u')
                        ->where("u.deletedAt IS NULL");
                },
                      
            ])
            ->handleRequest($request);

        if($userTable->isCallBack()){
            return $userTable->getResponse();
        }


        return $this->render('user/index.html.twig', [
            'table' => $userTable,
            'person_link' => true,
            'user_index' => true 
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods="GET|POST")
     */
    public function new(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /*
            $user->setIdentificationNo($data["identificationNo"]);
            $user->setIdentificationType($data["identificationType"]);
            $user->setName($data["name"]);
            $user->setEmail($data["email"]);
            $user->setTelefonNumber($data["telefonNumber"]);
            $user->setCurrentAddress($data["currentAddress"]);
            $user->setReferenceName($data["referenceName"]);
            $user->setReferenceNumber($data["referenceNumber"]);
            $user->setReferenceRelationship($data["referenceRelationship"]);
            $user->setStatus($data["status"]);
            $user->setRoles($data["roles"]);
            $encoded = $encoder->encodePassword($user,$data["password"]);
            $user->setPassword($encoded);*/
            $em = $this->getDoctrine()->getManager();
            $encoded = $encoder->encodePassword($user,$user->getPassword());
            $user->setPassword($encoded);
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('user_index');
            
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{personId}", name="user_show", methods="GET")
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', ['user' => $user]);
    }

     /**
     * @Route("/changePassword/{personId}", name="change_user_password", methods="GET|POST")
     */
    public function changePassword(Request $request,User $user, UserPasswordEncoderInterface $encoder): Response
    {   
        $error = null;
        if($request->get('password') == $request->get('confirmPassword')){
              $encoded = $encoder->encodePassword($user,$request->get('password'));
                $user->setPassword($encoded);
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute('user_show', ['personId' => $user->getPersonId()]);
        }else{
            $error = "Kata laluan tidak sama";
        }

        return $this->render('user/change_password.html.twig', 
            [
                'user' => $user,
                'error' => $error
        ]);
    }

    /**
     * @Route("/{personId}/edit", name="user_edit", methods="GET|POST")
     */
    public function edit(Request $request, User $user, UserPasswordEncoderInterface $encoder): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $encoded = $encoder->encodePassword($user,$user->getPassword());
            $user->setPassword($encoded);
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_edit', ['personId' => $user->getPersonId()]);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{personId}", name="user_delete", methods="DELETE")
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getPersonId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('user_index');
    }
}

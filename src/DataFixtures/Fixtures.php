<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Owner;
use App\Entity\Home;
use App\Entity\User;
use App\Entity\Contract;
use App\Entity\Tenant;
use App\Entity\Room;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Fixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {

        /*
            
            System Data

         */
        //Test Owner
        $owner = new Owner();
        $owner->setIdentificationNo("950511121111");
        $owner->setIdentificationType("IC");
    	$owner->setName("VaraOwner");
    	$owner->setEmail("mwara95@gmail.com");
    	$owner->setTelefonNumber("0060148648630");
    	$owner->setCurrentAddress(
            "Kampung Banting,
            Sindumin,
            89850 Sipitang,
            Sabah"
        );
    	$owner->setReferenceName("Vara 2");
    	$owner->setReferenceNumber("0061234123123");
    	$owner->setReferenceRelationship("Clone");

    	$manager->persist($owner);

        //Test Home
        $home = new Home();
        $home->setNumber("A2309");
        $home->setName("Paragon Suite");
        $home->setHomeAddress(
            "Jalan Inderaputra,
            Stulang Laut, 
            80300 Johor Bahru, 
            Johor");
        $home->setHomeType("Kondomonium");

        $manager->persist($home);

        $manager->flush();

        //Test Contract
        $homeContract = new Contract();
        $homeContract->setStartDate(new \DateTime('NOW'));
        $homeContract->setEndDate(new \DateTime('NOW'));
        $homeContract->setRentAmount(1500);
        $homeContract->setRentDeposit(300);
        $homeContract->setRentUtility(500);
        $homeContract->setRentFrequency('Monthly');
        $homeContract->setContractType("Owner Rental Aggrement");
        $homeContract->setRenewal(false);
        $homeContract->setPerson($owner);
        $homeContract->setPlace($home);

        $manager->persist($homeContract);

        $manager->flush();

        $room1 = new Room();
        $room1->setNumber("Room 1");
        $room1->setRoomType("Single");
        $room1->setCapacity(1);

        $room2 = new Room();
        $room2->setNumber("Room 2");
        $room2->setRoomType("Double");
        $room2->setCapacity(2);

        $room3 = new Room();
        $room3->setNumber("Room 3");
        $room3->setRoomType("Triple");
        $room3->setCapacity(3);

        $manager->persist($room1);
        $manager->persist($room2);
        $manager->persist($room3);

        $home->addRoom($room1);
        $home->addRoom($room2);
        $home->addRoom($room3);

        $manager->persist($home);
        $manager->flush();

        //Test Tenant 1
        $tenant1 = new Tenant();
        $tenant1->setIdentificationNo("950511125555");
        $tenant1->setIdentificationType("IC");
        $tenant1->setName("Hijau bin Mahsuri");
        $tenant1->setEmail("mwara95@gmail.com");
        $tenant1->setTelefonNumber("0060148648630");
        $tenant1->setCurrentAddress(
            "Lot 73, Lorong Enggang Ulu,
             Kelang Free Trade Zone,
             54200 Wilayah Persekutuan Kuala Lumpur"
        );
        $tenant1->setReferenceName("Vara 2");
        $tenant1->setReferenceNumber("0061234123123");
        $tenant1->setReferenceRelationship("Clone");

        //Test Tenant 2
        $tenant2 = new Tenant();
        $tenant2->setIdentificationNo("950511126666");
        $tenant2->setIdentificationType("IC");
        $tenant2->setName("Jaya binti Osman");
        $tenant2->setEmail("mwara95@gmail.com");
        $tenant2->setTelefonNumber("0060148648630");
        $tenant2->setCurrentAddress(
            "Khoon Lin Court, Jln Pasar,
            Kuala Lumpur,
            55100 Wilayah Persekutuan Kuala Lumpur"
        );
        $tenant2->setReferenceName("Vara 2");
        $tenant2->setReferenceNumber("0061234123123");
        $tenant2->setReferenceRelationship("Clone");

        //Test Tenant 3
        $tenant3 = new Tenant();
        $tenant3->setIdentificationNo("950511127777");
        $tenant3->setIdentificationType("IC");
        $tenant3->setName("Putri binti AdiPutri");
        $tenant3->setEmail("mwara95@gmail.com");
        $tenant3->setTelefonNumber("0060148648630");
        $tenant3->setCurrentAddress(
            "No 70-3, Wisma Mahamewah,
            Jalan Sungei Besi, 
            57100 Wilayah Persekutuan Kuala Lumpur"
        );
        $tenant3->setReferenceName("Vara 2");
        $tenant3->setReferenceNumber("0061234123123");
        $tenant3->setReferenceRelationship("Clone");

        //Test Tenant 4
        $tenant4 = new Tenant();
        $tenant4->setIdentificationNo("950511128888");
        $tenant4->setIdentificationType("IC");
        $tenant4->setName("Sri bin Nilam");
        $tenant4->setEmail("mwara95@gmail.com");
        $tenant4->setTelefonNumber("0060148648630");
        $tenant4->setCurrentAddress(
            "Lot 10669, Jalan Kuchai Lama,
            Kuala Lumpur,
            57100 Wilayah Persekutuan Kuala Lumpur"
        );
        $tenant4->setReferenceName("Vara 2");
        $tenant4->setReferenceNumber("0061234123123");
        $tenant4->setReferenceRelationship("Clone");

        //Test Tenant 5
        $tenant5 = new Tenant();
        $tenant5->setIdentificationNo("950511129999");
        $tenant5->setIdentificationType("IC");
        $tenant5->setName("Mayang bin Putih");
        $tenant5->setEmail("mwara95@gmail.com");
        $tenant5->setTelefonNumber("0060148648630");
        $tenant5->setCurrentAddress(
            "143, Jalan Maharajalela, 
             Kuala Lumpur,
             50150 Wilayah Persekutuan Kuala Lumpur"
        );
        $tenant5->setReferenceName("Vara 2");
        $tenant5->setReferenceNumber("0061234123123");
        $tenant5->setReferenceRelationship("Clone");

        //Test Tenant 6
        $tenant6 = new Tenant();
        $tenant6->setIdentificationNo("950511130000");
        $tenant6->setIdentificationType("IC");
        $tenant6->setName("Nirmala bin SayangNirmala");
        $tenant6->setEmail("mwara95@gmail.com");
        $tenant6->setTelefonNumber("0060148648630");
        $tenant6->setCurrentAddress(
            "11, Menara Phileo, 
            Jalan Tun Razak,
            Kuala Lumpur,
            50400 Wilayah Persekutuan Kuala Lumpur"
        );
        $tenant6->setReferenceName("Vara 2");
        $tenant6->setReferenceNumber("0061234123123");
        $tenant6->setReferenceRelationship("Clone");

        //Test Tenant 7
        $tenant7 = new Tenant();
        $tenant7->setIdentificationNo("950511131111");
        $tenant7->setIdentificationType("IC");
        $tenant7->setName("Esah binti Puteh");
        $tenant7->setEmail("mwara95@gmail.com");
        $tenant7->setTelefonNumber("0060148648630");
        $tenant7->setCurrentAddress(
            "Lot 18115, Batu 5, 
            Jalan Kelang Lama,
            Kuala Lumpur,
            50400 Wilayah Persekutuan Kuala Lumpur"
        );
        $tenant7->setReferenceName("Vara 2");
        $tenant7->setReferenceNumber("0061234123123");
        $tenant7->setReferenceRelationship("Clone");

        //Test Tenant 8
        $tenant8 = new Tenant();
        $tenant8->setIdentificationNo("950511132222");
        $tenant8->setIdentificationType("IC");
        $tenant8->setName("Minah binti Yahaya");
        $tenant8->setEmail("mwara95@gmail.com");
        $tenant8->setTelefonNumber("0060148648630");
        $tenant8->setCurrentAddress(
            "4, Hotel Pan Pacific,
            Jln Putra, Kuala Lumpur,
            50350 Wilayah Persekutuan Kuala Lumpur"
        );
        $tenant8->setReferenceName("Vara 2");
        $tenant8->setReferenceNumber("0061234123123");
        $tenant8->setReferenceRelationship("Clone");

        $manager->persist($tenant1);
        $manager->persist($tenant2);
        $manager->persist($tenant3);
        $manager->persist($tenant4);
        $manager->persist($tenant5);
        $manager->persist($tenant6);
        $manager->persist($tenant7);
        $manager->persist($tenant8);

        $manager->flush();

        $roomContract1 = new Contract();
        $roomContract1->setStartDate(new \DateTime('2018-01-01'));
        $roomContract1->setEndDate(new \DateTime('2018-05-30'));
        $roomContract1->setRentAmount(600);
        $roomContract1->setRentDeposit(900);
        $roomContract1->setRentUtility(100);
        $roomContract1->setRentFrequency('Monthly');
        $roomContract1->setContractType("Tenant Rental Aggrement");
        $roomContract1->setRenewal(false);
        $roomContract1->setPerson($tenant1);
        $roomContract1->setPlace($room1);

        $roomContract2 = new Contract();
        $roomContract2->setStartDate(new \DateTime('2018-03-05'));
        $roomContract2->setEndDate(new \DateTime('2018-05-04'));
        $roomContract2->setRentAmount(300);
        $roomContract2->setRentDeposit(450);
        $roomContract2->setRentUtility(100);
        $roomContract2->setRentFrequency('Monthly');
        $roomContract2->setContractType("Tenant Rental Aggrement");
        $roomContract2->setRenewal(false);
        $roomContract2->setPerson($tenant2);
        $roomContract2->setPlace($room2);

        $roomContract3 = new Contract();
        $roomContract3->setStartDate(new \DateTime('2017-01-01'));
        $roomContract3->setEndDate(new \DateTime('2017-06-30'));
        $roomContract3->setRentAmount(300);
        $roomContract3->setRentDeposit(450);
        $roomContract3->setRentUtility(100);
        $roomContract3->setRentFrequency('Monthly');
        $roomContract3->setContractType("Tenant Rental Aggrement");
        $roomContract3->setRenewal(false);
        $roomContract3->setPerson($tenant3);
        $roomContract3->setPlace($room2);

        $roomContract4 = new Contract();
        $roomContract4->setStartDate(new \DateTime('2017-05-07'));
        $roomContract4->setEndDate(new \DateTime('2017-05-06'));
        $roomContract4->setRentAmount(150);
        $roomContract4->setRentDeposit(225);
        $roomContract4->setRentUtility(100);
        $roomContract4->setRentFrequency('Monthly');
        $roomContract4->setContractType("Tenant Rental Aggrement");
        $roomContract4->setRenewal(false);
        $roomContract4->setPerson($tenant4);
        $roomContract4->setPlace($room3);

        $roomContract5 = new Contract();
        $roomContract5->setStartDate(new \DateTime('2018-01-01'));
        $roomContract5->setEndDate(new \DateTime('2018-12-12'));
        $roomContract5->setRentAmount(150);
        $roomContract5->setRentDeposit(225);
        $roomContract5->setRentUtility(100);
        $roomContract5->setRentFrequency('Monthly');
        $roomContract5->setContractType("Tenant Rental Aggrement");
        $roomContract5->setRenewal(false);
        $roomContract5->setPerson($tenant5);
        $roomContract5->setPlace($room3);

        $manager->persist($roomContract1);
        $manager->persist($roomContract2);
        $manager->persist($roomContract3);
        $manager->persist($roomContract4);
        $manager->persist($roomContract5);

        $manager->flush();

        /*
        
        System Users

         */
        //Test Manager
        $userManager = new User();
        $userManager->setIdentificationNo("950511122222");
        $userManager->setIdentificationType("IC");
        $userManager->setName("ManagerVara");
        $userManager->setEmail("mwara95@gmail.com");
        $userManager->setTelefonNumber("0060148648630");
        $userManager->setCurrentAddress(
            "Kampung Banting,
            Sindumin,
            89850 Sipitang,
            Sabah"
        );
        $userManager->setReferenceName("Vara 2");
        $userManager->setReferenceNumber("0061234123123");
        $userManager->setReferenceRelationship("Clone");
        $userManager->setStatus(1);
        $userManager->setRoles(array("ROLE_MANAGER"));
        //Set Password
        $encoded = $this->encoder->encodePassword($userManager,"manager");
        $userManager->setPassword($encoded);


         //Test Admin
        $userAdmin = new User();
        $userAdmin->setIdentificationNo("950511123333");
        $userAdmin->setIdentificationType("IC");
        $userAdmin->setName("AdminVara");
        $userAdmin->setEmail("mwara95@gmail.com");
        $userAdmin->setTelefonNumber("0060148648630");
        $userAdmin->setCurrentAddress(
            "Kampung Banting,
            Sindumin,
            89850 Sipitang,
            Sabah"
        );
        $userAdmin->setReferenceName("Vara 2");
        $userAdmin->setReferenceNumber("0061234123123");
        $userAdmin->setReferenceRelationship("Clone");
        $userAdmin->setPassword("admin");
        $userAdmin->setStatus(1);
        $userAdmin->setRoles(array("ROLE_ADMIN"));
        //Set Password
        $encoded = $this->encoder->encodePassword($userAdmin,"admin");
        $userAdmin->setPassword($encoded);

         //Test Sales
        $userSales = new User();
        $userSales->setIdentificationNo("950511124444");
        $userSales->setIdentificationType("IC");
        $userSales->setName("SalesVara");
        $userSales->setEmail("mwara95@gmail.com");
        $userSales->setTelefonNumber("0060148648630");
        $userSales->setCurrentAddress(
            "Kampung Banting,
            Sindumin,
            89850 Sipitang,
            Sabah"
        );
        $userSales->setReferenceName("Vara 2");
        $userSales->setReferenceNumber("0061234123123");
        $userSales->setReferenceRelationship("Clone");
        $userSales->setStatus(1);
        $userSales->setRoles(array("ROLE_SALES"));
        //Set Password
        $encoded = $this->encoder->encodePassword($userSales,"sales");
        $userSales->setPassword($encoded);

        $manager->persist($userManager);
        $manager->persist($userAdmin);
        $manager->persist($userSales);
        
        $manager->flush();
    }
}

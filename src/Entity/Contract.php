<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContractRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class Contract
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $actualEndDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $noticeDate;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\Column(type="float")
     */
    private $rentAmount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rentFrequency;

    /**
     * @ORM\Column(type="float")
     */
    private $rentDeposit;

    /**
     * @ORM\Column(type="float")
     */
    private $rentUtility;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contractType;

    /**
     * @ORM\Column(type="boolean")
     */
    private $renewal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="contracts")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="person_id",nullable=false)
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Place", inversedBy="contracts")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="place_id",nullable=false)
     */
    private $place;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;


    public function __construct()
    {

    }

    public function getId()
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getActualEndDate(): ?\DateTimeInterface
    {
        return $this->actualEndDate;
    }

    public function setActualEndDate(?\DateTimeInterface $actualEndDate): self
    {
        $this->actualEndDate = $actualEndDate;

        return $this;
    }

    public function getNoticeDate(): ?\DateTimeInterface
    {
        return $this->noticeDate;
    }

    public function setNoticeDate(?\DateTimeInterface $noticeDate): self
    {
        $this->noticeDate = $noticeDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getRentAmount(): ?float
    {
        return $this->rentAmount;
    }

    public function setRentAmount(float $rentAmount): self
    {
        $this->rentAmount = $rentAmount;

        return $this;
    }

    public function getRentFrequency(): ?string
    {
        return $this->rentFrequency;
    }

    public function setRentFrequency(string $rentFrequency): self
    {
        $this->rentFrequency = $rentFrequency;

        return $this;
    }


    public function getRentDeposit(): ?float
    {
        return $this->rentDeposit;
    }

    public function setRentDeposit(float $rentDeposit): self
    {
        $this->rentDeposit = $rentDeposit;

        return $this;
    }

    public function getRentUtility(): ?float
    {
        return $this->rentUtility;
    }

    public function setRentUtility(float $rentUtility): self
    {
        $this->rentUtility = $rentUtility;

        return $this;
    }

    public function getContractType(): ?string
    {
        return $this->contractType;
    }

    public function setContractType(string $contractType): self
    {
        $this->contractType = $contractType;

        return $this;
    }

    public function getRenewal(): ?bool
    {
        return $this->renewal;
    }

    public function setRenewal(bool $renewal): self
    {
        $this->renewal = $renewal;

        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getContentChanged()
    {
        return $this->contentChanged;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getPlace(): ?Place
    {
        return $this->place;
    }

    public function setPlace(?Place $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}

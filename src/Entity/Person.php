<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Util\ClassUtils;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn("person_type")
 * @ORM\DiscriminatorMap({"person" = "Person","owner" = "Owner", "tenant" = "Tenant","user"="User"})
 */
class Person {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $personId;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $identificationNo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $identificationType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telefonNumber;

    /**
     * @ORM\Column(type="text")
     */
    private $currentAddress;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $referenceName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $referenceNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $referenceRelationship;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contract", mappedBy="person")
     * @ORM\OrderBy({"startDate"="ASC"})
     */
    private $contracts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="person")
     */
    private $transactions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Invoice", mappedBy="person", orphanRemoval=true)
     */
    private $invoices;

    
    public function __construct()
    {
        $this->contracts = new ArrayCollection();
        $this->transactions = new ArrayCollection();
        $this->invoices = new ArrayCollection();
    }

    public function getPersonId()
    {
        return $this->personId;
    }

    public function getLastContract()
    {
        return $this->contracts->last();
    }

    public function getIdentificationNo(): ?string
    {
        return $this->identificationNo;
    }

    public function setIdentificationNo(string $identificationNo): self
    {
        $this->identificationNo = $identificationNo;

        return $this;
    }

    public function getIdentificationType(): ?string
    {
        return $this->identificationType;
    }

    public function setIdentificationType(string $identificationType): self
    {
        $this->identificationType = $identificationType;

        return $this;
    }

    public function getIcAndName(): ?string
    {
        return "( ".$this->identificationNo." ) ".$this->name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelefonNumber(): ?string
    {
        return $this->telefonNumber;
    }

    public function setTelefonNumber(string $telefonNumber): self
    {
        $this->telefonNumber = $telefonNumber;

        return $this;
    }

    public function getCurrentAddress(): ?string
    {
        return $this->currentAddress;
    }

    public function setCurrentAddress(string $currentAddress): self
    {
        $this->currentAddress = $currentAddress;

        return $this;
    }

    public function getReferenceName(): ?string
    {
        return $this->referenceName;
    }

    public function setReferenceName(string $referenceName): self
    {
        $this->referenceName = $referenceName;

        return $this;
    }

    public function getReferenceNumber(): ?string
    {
        return $this->referenceNumber;
    }

    public function setReferenceNumber(string $referenceNumber): self
    {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    public function getReferenceRelationship(): ?string
    {
        return $this->referenceRelationship;
    }

    public function setReferenceRelationship(string $referenceRelationship): self
    {
        $this->referenceRelationship = $referenceRelationship;

        return $this;
    }

    public function getClass(){

        return ClassUtils::getClass($this);

    }

    /**
     * @return Collection|Contract[]
     */
    public function getContracts(): Collection
    {
        return $this->contracts;
    }

    public function addContract(Contract $contract): self
    {
        if (!$this->contracts->contains($contract)) {
            $this->contracts[] = $contract;
            $contract->setPerson($this);
        }

        return $this;
    }

    public function removeContract(Contract $contract): self
    {
        if ($this->contracts->contains($contract)) {
            $this->contracts->removeElement($contract);
            // set the owning side to null (unless already changed)
            if ($contract->getPerson() === $this) {
                $contract->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setPerson($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            if ($transaction->getPerson() === $this) {
                $transaction->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Invoice[]
     */
    public function getInvoices(): Collection
    {
        return $this->invoices;
    }

    public function addInvoice(Invoice $invoice): self
    {
        if (!$this->invoices->contains($invoice)) {
            $this->invoices[] = $invoice;
            $invoice->setPerson($this);
        }

        return $this;
    }

    public function removeInvoice(Invoice $invoice): self
    {
        if ($this->invoices->contains($invoice)) {
            $this->invoices->removeElement($invoice);
            // set the owning side to null (unless already changed)
            if ($invoice->getPerson() === $this) {
                $invoice->setPerson(null);
            }
        }

        return $this;
    }

    public function __toString(){
        return (string)$this->getPersonId();
    }

}

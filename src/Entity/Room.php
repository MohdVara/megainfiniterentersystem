<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoomRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class Room extends Place
{

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $roomType;

    /**
     * @ORM\Column(type="integer")
     */
    private $capacity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Home", inversedBy="rooms")
     * @ORM\JoinColumn(name="home_id", referencedColumnName="place_id",nullable=false)
     * @ORM\JoinColumn(nullable=false)
     */
    private $home;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;


    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    public function getFullRoomAddress(): ?string
    {
        return $this->getNumber()." ,".$this->home->getHomeNumberAndName();
    }


    public function getRoomType(): ?string
    {
        return $this->roomType;
    }

    public function setRoomType(string $roomType): self
    {
        $this->roomType = $roomType;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getOccupantOnYear($year) {
        $currentDate = new \Datetime("now");
        $thirtyDaysLater = clone $currentDate;
        date_modify($thirtyDaysLater,"+60 days");
        $occupants = array();
        $contracts = $this->getContracts();
        foreach ($contracts as $contract) {
            if($contract->getStartDate()->format('Y') == $year && 
               $contract->getEndDate() < $currentDate){
                    array_push($occupants, $contract->getPerson());    
                }
        }

        array_unique($occupants);
        return $occupants;
    }

    public function getOccupant() {
        $currentDate = new \Datetime("now");
        $thirtyDaysLater = clone $currentDate;
        date_modify($thirtyDaysLater,"+60 days");
        $occupants = array();
        $contracts = $this->getContracts();
        foreach ($contracts as $contract) {
            if($contract->getStartDate() < $thirtyDaysLater && $contract->getEndDate() > $currentDate && $contract->getActualEndDate() == null){
                    array_push($occupants, $contract->getPerson());    
                }
        }

        return $occupants;
    }

    public function getCurrentOccupant(){
        $currentDate = new \Datetime("now");
        $thirtyDaysLater = clone $currentDate;
        date_modify($thirtyDaysLater,"+60 days");
        $occupants = array();
        $contracts = $this->getContracts();
        foreach ($contracts as $contract) {
            if($contract->getStartDate() < $thirtyDaysLater && $contract->getEndDate() > $currentDate){
                array_push($occupants, $contract->getPerson());    
            }
        }

        array_unique($occupants);
        return $occupants;
    }

    public function getHome(): ?Home
    {
        return $this->home;
    }

    public function setHome(?Home $home): self
    {
        $this->home = $home;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setRoom($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            if ($transaction->getRoom() === $this) {
                $transaction->setRoom(null);
            }
        }

        return $this;
    }

}

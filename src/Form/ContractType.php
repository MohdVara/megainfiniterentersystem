<?php

namespace App\Form;

use App\Entity\Contract;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ContractType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', DateType::class, array(
                          'label' => 'Tarikh Mula',
                          'html5' => true,
                         'widget' => 'single_text'
                    )
            )
            ->add('actualEndDate', DateType::class, array(
                          'label' => 'Tarikh Keluar',
                          'html5' => true,
                         'widget' => 'single_text'
                    )
            )
            ->add('endDate', DateType::class, array(
                          'label' => 'Tarikh Tamat',
                          'html5' => true,
                         'widget' => 'single_text'
                    )
            )
            ->add('rentDeposit', MoneyType::class, array(
                        'currency' => 'MYR'
                    )
            )
            ->add('rentUtility', MoneyType::class, array(
                        'currency' => 'MYR'
                    )
            )
            ->add('renewal', ChoiceType::class, array(
                'choices'  => array(
                      'Yes' => true,
                      'No' => false
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contract::class,
        ]);
    }
}

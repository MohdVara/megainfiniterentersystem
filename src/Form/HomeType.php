<?php

namespace App\Form;

use App\Entity\Home;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class HomeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', null, array(
                'label' => 'No Kediaman'
            ))
            ->add('name', null, array(
                'label' => 'Name Perumahan/Bangunan'
            ))
            ->add('homeAddress', TextareaType::class, array(
                'label' => 'Alamat',
                'attr' => array('class' => 'tinymce')
            ))
            ->add('homeType', null, array(
                'label' => 'Jenis Kediaman'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Home::class,
        ]);
    }
}

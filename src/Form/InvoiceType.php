<?php

namespace App\Form;

use App\Entity\Invoice;
use App\Entity\Person;
use App\Repository\PersonRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class InvoiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', TextareaType::class, array(
                'label' => 'Butiran Invoice'
            ))
            ->add('amount', MoneyType::class, array(
                        'label' => 'Total Bayaran',
                        'currency' => 'MYR'
                    )
            )
            ->add('invoiceType', ChoiceType::class, array(
                  'label' => 'Status',
                  'choices' => array(
                    'pembayaran' => 'DEBIT',
                   'pemberian' => 'CREDIT'
                )
            ))
            ->add('dateIssued', DateType::class, array(
                          'label' => 'Tarikh Dikeluarkan',
                          'html5' => true,
                         'widget' => 'single_text'

                    )
            )
            ->add('person', EntityType::class, array(
                    'label' => 'Pemberi / Penerima',
                    'class' => Person::class,   
                    'query_builder' => function (PersonRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->orderBy("p.name")
                                ;
                    },

                    // uses the User.username property as the visible option string
                    'choice_label' => 'icAndName'

                    // used to render a select box, check boxes or radios
                    // 'multiple' => true,
                    // 'expanded' => true,
            ))
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Invoice::class,
        ]);
    }
}

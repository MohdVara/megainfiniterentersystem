<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class NewHouseFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //Contract Details
             ->add('startDate', DateType::class, array(
                          'label' => 'Tarikh Mula',
                          'html5' => true,
                         'widget' => 'single_text'

                    )
            )
            ->add('endDate', DateType::class, array(
                          'label' => 'Tarikh Tamat',
                          'html5' => true,
                         'widget' => 'single_text'
                    )
            )
            ->add('rentAmount', MoneyType::class, array(
                        'label' => 'Amaun Sewa',
                        'currency' => 'MYR'
                    )
            )
            ->add('rentFrequency', ChoiceType::class, array(
                  'label' => 'Kekerapan Bayaran Sewa',
                'choices' => array(
                    'Bulanan' => 'Monthly',
                   'Mingguan' => 'Weekly',
                    'Tahunan' => 'Yearly'
                )
            ))
            ->add('rentDeposit', MoneyType::class, array(
                        'label' => 'Deposit Sewa',
                        'currency' => 'MYR'
                    )
            )
            ->add('rentUtility', MoneyType::class, array(
                        'label' => 'Deposit Utility',
                        'currency' => 'MYR'
                    )
            )
            //House Number
            ->add('number', null, array(
                'label' => 'No Kediaman'
            ))
            ->add('placeName', null, array(
                'label' => 'Nama Perumahan/Bangunan'
            ))
            ->add('homeAddress', TextareaType::class, array(
                'label' => 'Alamat'
            ))
            ->add('homeType', null, array(
                'label' => 'Jenis Kediaman'
            ))
            //Owner Details
            ->add('identificationNo', null, array(
                'label' => 'No Dokumen Pengenalan'
            ))
            ->add('identificationType', ChoiceType::class, array(
                  'label' => 'Jenis Dokumen Pengenalan',
                'choices' => array(
                    'IC' => 'IC',
                   'Passport' => 'Passport'
                )
            ))
            ->add('name', null, array(
                'label' => 'Nama'
            ))
            ->add('email', null, array(
                'label' => 'E-mel',
                'required' => 'false'
            ))
            ->add('telefonNumber', null, array(
                'label' => 'No Telefon'
            ))
            ->add('currentAddress', TextareaType::class, array(
                'label' => 'Alamat Surat Menyurat Terkini'
            ))
            ->add('referenceName', null, array(
                'label' => 'Nama Rujukan'
            ))
            ->add('referenceNumber', null, array(
                'label' => 'No Telefon Rujukan'
            ))
            ->add('referenceRelationship', null, array(
                'label' => 'Hubungan Rujukan'
            ))
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}

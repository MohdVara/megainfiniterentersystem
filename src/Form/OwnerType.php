<?php

namespace App\Form;

use App\Entity\Owner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OwnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('identificationNo', null, array(
                'label' => 'No Dokumen Pengenalan'
            ))
            ->add('identificationType', ChoiceType::class, array(
                  'label' => 'Jenis Dokumen Pengenalan',
                'choices' => array(
                    'IC' => 'IC',
                   'Passport' => 'Passport'
                )
            ))
            ->add('name', null, array(
                'label' => 'Nama'
            ))
            ->add('email', null, array(
                'label' => 'E-mel',
                'required' => 'false'
            ))
            ->add('telefonNumber', null, array(
                'label' => 'No Telefon'
            ))
            ->add('currentAddress', TextareaType::class, array(
                'label' => 'Alamat Surat Menyurat Terkini'
            ))
            ->add('referenceName', null, array(
                'label' => 'Name Rujukan'
            ))
            ->add('referenceNumber', null, array(
                'label' => 'No Telefon Rujukan'
            ))
            ->add('referenceRelationship', null, array(
                'label' => 'Hubungan Rujukan'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Owner::class,
        ]);
    }
}

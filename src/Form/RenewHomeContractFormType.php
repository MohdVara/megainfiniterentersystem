<?php

namespace App\Form;

use App\Entity\Owner;
use App\Entity\Home;
use App\Repository\HomeRepository;
use App\Repository\OwnerRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RenewHomeContractFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //Contract Details
             ->add('startDate', DateType::class, array(
                          'label' => 'Tarikh Mula',
                          'html5' => true,
                         'widget' => 'single_text'

                    )
            )
            ->add('endDate', DateType::class, array(
                          'label' => 'Tarikh Tamat',
                          'html5' => true,
                         'widget' => 'single_text'
                    )
            )
            ->add('rentAmount', MoneyType::class, array(
                        'label' => 'Amaun Sewa',
                        'currency' => 'MYR'
                    )
            )
            ->add('rentFrequency', ChoiceType::class, array(
                  'label' => 'Kekerapan Bayaran Sewa',
                'choices' => array(
                    'Bulanan' => 'Monthly',
                   'Mingguan' => 'Weekly',
                    'Tahunan' => 'Yearly'
                )
            ))
            ->add('rentDeposit', MoneyType::class, array(
                        'label' => 'Deposit Sewa',
                        'currency' => 'MYR'
                    )
            )
            ->add('rentUtility', MoneyType::class, array(
                        'label' => 'Deposit Utility',
                        'currency' => 'MYR'
                    )
            )
            ->add('owner', EntityType::class, array(
                    'label' => 'Pemilik',
                    'class' => Owner::class,
                    'query_builder' => function (OwnerRepository $er) {
                        return $er->createQueryBuilder('o')
                                ->andWhere("o.deletedAt is null")
                                ->orderBy("o.name")
                                ;
                    },

                    // uses the User.username property as the visible option string
                    'choice_label' => 'IcAndName'

                    // used to render a select box, check boxes or radios
                    // 'multiple' => true,
                    // 'expanded' => true,
            ))
            /*
             ->add('Home', EntityType::class, array(
                    'label' => 'Rumah',
                    'class' => Home::class,   
                    'query_builder' => function (HomeRepository $er) {
                        return $er->createQueryBuilder('h')
                                ->andWhere("h.deletedAt is null")
                                ->orderBy("h.number")
                                ;
                    },

                    // uses the User.username property as the visible option string
                    'choice_label' => 'HomeNumberAndName'

                    // used to render a select box, check boxes or radios
                    // 'multiple' => true,
                    // 'expanded' => true,
            ))
            */;

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}

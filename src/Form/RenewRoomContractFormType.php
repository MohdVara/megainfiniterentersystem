<?php

namespace App\Form;

use App\Entity\Tenant;
use App\Repository\TenantRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class RenewRoomContractFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //Contract Details
             ->add('startDate', DateType::class, array(
                          'label' => 'Tarikh Mula',
                          'html5' => true,
                         'widget' => 'single_text'

                    )
            )
            ->add('endDate', DateType::class, array(
                          'label' => 'Tarikh Tamat',
                          'html5' => true,
                         'widget' => 'single_text'
                    )
            )
            ->add('rentAmount', MoneyType::class, array(
                        'label' => 'Amaun Sewa',
                        'currency' => 'MYR'
                    )
            )
            ->add('rentFrequency', ChoiceType::class, array(
                  'label' => 'Kekerapan Bayaran Sewa',
                'choices' => array(
                    'Bulanan' => 'Monthly',
                   'Mingguan' => 'Weekly',
                    'Tahunan' => 'Yearly'
                )
            ))
            ->add('rentDeposit', MoneyType::class, array(
                        'label' => 'Deposit Sewa',
                        'currency' => 'MYR'
                    )
            )
            ->add('rentUtility', MoneyType::class, array(
                        'label' => 'Deposit Utility',
                        'currency' => 'MYR'
                    )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}

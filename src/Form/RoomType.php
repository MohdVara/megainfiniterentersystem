<?php

namespace App\Form;

use App\Entity\Home;
use App\Entity\Room;
use App\Repository\HomeRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RoomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', null, array(
                'label' => 'No Bilik'
            ))
            ->add('roomType', null, array(
                'label' => 'Jenis Bilik'
            ))
            ->add('capacity', null, array(
                'label' => 'Bilangan Orang'
            ))
             ->add('Home', EntityType::class, array(
                    'label' => 'Rumah',
                    'class' => Home::class,   
                    'query_builder' => function (HomeRepository $er) {
                        return $er->createQueryBuilder('h')
                                ->andWhere("h.deletedAt is null")
                                ->orderBy("h.number")
                                ;
                    },

                    // uses the User.username property as the visible option string
                    'choice_label' => 'HomeNumberAndName'

                    // used to render a select box, check boxes or radios
                    // 'multiple' => true,
                    // 'expanded' => true,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Room::class,
        ]);
    }
}

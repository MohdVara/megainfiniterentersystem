<?php

namespace App\Form;

use App\Entity\Home;
use App\Entity\Room;
use App\Repository\HomeRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RoomWithHomeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', null, array(
                'label' => 'No Bilik'
            ))
            ->add('roomType', null, array(
                'label' => 'Jenis Bilik'
            ))
            ->add('capacity', null, array(
                'label' => 'Bilangan Orang'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}

<?php

namespace App\Form;

use App\Entity\Contract;
use App\Entity\Room;
use App\Entity\Tenant;
use App\Repository\TenantRepository;
use App\Repository\RoomRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class TenantContractType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
              ->add('tenant', EntityType::class, array(
                    'label' => 'Penyewa',
                    'class' => Tenant::class,
                    'query_builder' => function (TenantRepository $er) {
                        return $er->createQueryBuilder('t')
                                ->andWhere("t.deletedAt is null")
                                ->orderBy("t.name")
                                ;
                    },

                    // uses the User.username property as the visible option string
                    'choice_label' => 'IcAndName'

                    // used to render a select box, check boxes or radios
                    // 'multiple' => true,
                    // 'expanded' => true,
            ))
            //Room
             ->add('room', EntityType::class, array(
                    'label' => 'Bilik',
                    'class' => Room::class,   
                    'query_builder' => function (RoomRepository $er) {
                        return $er->createQueryBuilder('r')
                                ->andWhere("r.deletedAt is null")
                                ->orderBy("r.number")
                                ;
                    },

                    // uses the User.username property as the visible option string
                    'choice_label' => 'FullRoomAddress'

                    // used to render a select box, check boxes or radios
                    // 'multiple' => true,
                    // 'expanded' => true,
            ))
            ->add('startDate', DateType::class, array(
                          'label' => 'Tarikh Mula',
                          'html5' => true,
                         'widget' => 'single_text'
                    )
            )
            ->add('actualEndDate', DateType::class, array(
                          'label' => 'Tarikh Keluar',
                          'html5' => true,
                         'widget' => 'single_text',
                         'required' => false
                    )
            )
            ->add('noticeDate', DateType::class, array(
                          'label' => 'Tarikh Notifikasi Keluar',
                          'html5' => true,
                         'widget' => 'single_text',
                         'required' => false

                    )
            )
            ->add('endDate', DateType::class, array(
                          'label' => 'Tarikh Tamat',
                          'html5' => true,
                         'widget' => 'single_text',
                         'required' => false
                    )
            )
            ->add('rentAmount', MoneyType::class, array(
                        'label' => 'Amaun Sewa',
                        'currency' => 'MYR'
                    )
            )
            ->add('rentFrequency', ChoiceType::class, array(
                  'label' => 'Kekerapan Bayaran Sewa',
                'choices' => array(
                    'Bulanan' => 'Monthly',
                   'Mingguan' => 'Weekly',
                    'Tahunan' => 'Yearly'
                )
            ))
            ->add('rentDeposit', MoneyType::class, array(
                        'label' => 'Deposit Sewa',
                        'currency' => 'MYR'
                    )
            )
            ->add('rentUtility', MoneyType::class, array(
                        'label' => 'Deposit Utility',
                        'currency' => 'MYR'
                    )
            )
            ->add('renewal', ChoiceType::class, array(
                'choices'  => array(
                      'Yes' => true,
                      'No' => false
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}

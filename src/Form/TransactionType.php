<?php

namespace App\Form;

use App\Entity\Transaction;
use App\Entity\Tenant;
use App\Entity\Owner;
use App\Entity\Person;
use App\Entity\Room;
use App\Repository\TenantRepository;
use App\Repository\PersonRepository;
use App\Repository\RoomRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class TransactionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
             ->add('person', EntityType::class, array(
                    'label' => 'Pemberi / Penerima',
                    'class' => Person::class,
                    'attr' => ['class' => 'selectpicker', 'data-live-search' => "true"],    
                    'query_builder' => function (PersonRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->leftJoin('App\Entity\Owner','op','WITH','p.personId = op.personId')
                                 ->leftJoin('App\Entity\Tenant','tp','WITH','p.personId = tp.personId')
                                ->where('p INSTANCE OF App\Entity\Owner')
                                ->orWhere('p INSTANCE OF App\Entity\Tenant')
                                ->andWhere('op.deletedAt is NULL')
                                ->andWhere('tp.deletedAt is NULL')
                                ->orderBy("p.name")
                                ;
                    },

                    // uses the User.username property as the visible option string
                    'choice_label' => 'icAndName'

                    // used to render a select box, check boxes or radios
                    // 'multiple' => true,
                    // 'expanded' => true,
            ))
            ->add('transactionDescription', null, array(
                'label' => 'Butiran Transaksi'
            ))
             ->add('transactionRemark', TextareaType::class, array(
                'label' => 'Catatan Pembayaran',
                'required' => false
            ))
            ->add('transactionAmount', MoneyType::class, array(
                        'currency' => 'MYR'
                    )
            )
            ->add('transactionType', ChoiceType::class, array(
                  'label' => 'Jenis Transaksi',
                'choices' => array(
                    'kemasukan' => 'DEBIT',
                   'pengeluaran' => 'CREDIT'
                )
            ))
            ->add('dateIssued', DateType::class, array(
                          'label' => 'Tarikh Dikeluarkan',
                          'html5' => true,
                         'widget' => 'single_text'
                    )
            )
             ->add('datePaid', DateType::class, array(
                          'label' => 'Tarikh DiBayar',
                          'html5' => true,
                         'widget' => 'single_text',
                         'required' => false
                    )
            )
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Transaction::class,
        ]);
    }
}

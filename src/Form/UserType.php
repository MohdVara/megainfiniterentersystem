<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('identificationNo', null, array(
                'label' => 'No Dokumen Pengenalan'
            ))
            ->add('identificationType', ChoiceType::class, array(
                  'label' => 'Jenis Dokumen Pengenalan',
                'choices' => array(
                    'IC' => 'IC',
                   'Passport' => 'Passport'
                )
            ))
            ->add('password', PasswordType::class, array(
                'label' => 'Kata Laluan'
            ))
            ->add('name', null, array(
                'label' => 'Nama'
            ))
            ->add('email', null, array(
                'label' => 'E-mel',
                'required' => 'false'
            ))
            ->add('telefonNumber', null, array(
                'label' => 'No Telefon'
            ))
            ->add('currentAddress', TextareaType::class, array(
                'label' => 'Alamat Surat Menyurat Terkini'
            ))
            ->add('referenceName', null, array(
                'label' => 'Nama Rujukan'
            ))
            ->add('referenceNumber', null, array(
                'label' => 'No Telefon Rujukan'
            ))
            ->add('referenceRelationship', null, array(
                'label' => 'Hubungan Rujukan'
            ))

            ->add('status', ChoiceType::class, [
                'choices' => [
                    'ACTIVE' => 'ACTIVE',
                    'INACTIVE' => 'INACTIVE'
                ]
            ])
            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'choices' => [
                    'Manager' => 'ROLE_MANAGER',
                    'Admin' => 'ROLE_ADMIN',
                    'Sales' => 'SALES'
                    // ...
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

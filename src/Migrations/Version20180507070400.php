<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180507070400 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE transaction (id INT AUTO_INCREMENT NOT NULL, transaction_amount DOUBLE PRECISION NOT NULL, transaction_type VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, date_issued DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person (person_id INT AUTO_INCREMENT NOT NULL, identification_no VARCHAR(255) NOT NULL, identification_type VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, telefon_number VARCHAR(255) NOT NULL, current_address LONGTEXT NOT NULL, reference_name VARCHAR(255) NOT NULL, reference_number VARCHAR(255) NOT NULL, reference_relationship VARCHAR(255) NOT NULL, person_type VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_34DCD176956D75CA (identification_no), PRIMARY KEY(person_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (person_id INT NOT NULL, username VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, salt VARCHAR(255) DEFAULT NULL, status VARCHAR(255) NOT NULL, roles JSON NOT NULL COMMENT \'(DC2Type:json_array)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(person_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contract (id INT AUTO_INCREMENT NOT NULL, person_id INT NOT NULL, place_id INT NOT NULL, start_date DATETIME NOT NULL, actual_end_date DATETIME DEFAULT NULL, end_date DATETIME DEFAULT NULL, rent_amount DOUBLE PRECISION NOT NULL, rent_frequency VARCHAR(255) NOT NULL, rent_deposit DOUBLE PRECISION NOT NULL, rent_utility DOUBLE PRECISION NOT NULL, contract_type VARCHAR(255) NOT NULL, renewal TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_E98F2859217BBB47 (person_id), INDEX IDX_E98F2859DA6A219 (place_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contract_template (id INT AUTO_INCREMENT NOT NULL, template_name VARCHAR(255) NOT NULL, template_content LONGTEXT NOT NULL, created DATETIME NOT NULL, updated DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE place (place_id INT AUTO_INCREMENT NOT NULL, number VARCHAR(255) NOT NULL, place_type VARCHAR(255) NOT NULL, PRIMARY KEY(place_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE home (place_id INT NOT NULL, home_address LONGTEXT NOT NULL, home_type VARCHAR(255) NOT NULL, name VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(place_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE invoice (id INT AUTO_INCREMENT NOT NULL, status VARCHAR(255) NOT NULL, invoice_type VARCHAR(255) NOT NULL, date_issued DATETIME NOT NULL, amount DOUBLE PRECISION NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE owner (person_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(person_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE room (place_id INT NOT NULL, home_id INT NOT NULL, room_type VARCHAR(255) NOT NULL, capacity INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_729F519B28CDC89C (home_id), PRIMARY KEY(place_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tenant (person_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(person_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_translations (id INT AUTO_INCREMENT NOT NULL, locale VARCHAR(8) NOT NULL, object_class VARCHAR(255) NOT NULL, field VARCHAR(32) NOT NULL, foreign_key VARCHAR(64) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX translations_lookup_idx (locale, object_class, foreign_key), UNIQUE INDEX lookup_unique_idx (locale, object_class, field, foreign_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB ROW_FORMAT = DYNAMIC');
        $this->addSql('CREATE TABLE ext_log_entries (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(255) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', username VARCHAR(255) DEFAULT NULL, INDEX log_class_lookup_idx (object_class), INDEX log_date_lookup_idx (logged_at), INDEX log_user_lookup_idx (username), INDEX log_version_lookup_idx (object_id, object_class, version), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB ROW_FORMAT = DYNAMIC');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649217BBB47 FOREIGN KEY (person_id) REFERENCES person (person_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859217BBB47 FOREIGN KEY (person_id) REFERENCES person (person_id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859DA6A219 FOREIGN KEY (place_id) REFERENCES place (place_id)');
        $this->addSql('ALTER TABLE home ADD CONSTRAINT FK_71D60CD0DA6A219 FOREIGN KEY (place_id) REFERENCES place (place_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE owner ADD CONSTRAINT FK_CF60E67C217BBB47 FOREIGN KEY (person_id) REFERENCES person (person_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519B28CDC89C FOREIGN KEY (home_id) REFERENCES home (place_id)');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519BDA6A219 FOREIGN KEY (place_id) REFERENCES place (place_id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tenant ADD CONSTRAINT FK_4E59C462217BBB47 FOREIGN KEY (person_id) REFERENCES person (person_id) ON DELETE CASCADE');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649217BBB47');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F2859217BBB47');
        $this->addSql('ALTER TABLE owner DROP FOREIGN KEY FK_CF60E67C217BBB47');
        $this->addSql('ALTER TABLE tenant DROP FOREIGN KEY FK_4E59C462217BBB47');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F2859DA6A219');
        $this->addSql('ALTER TABLE home DROP FOREIGN KEY FK_71D60CD0DA6A219');
        $this->addSql('ALTER TABLE room DROP FOREIGN KEY FK_729F519BDA6A219');
        $this->addSql('ALTER TABLE room DROP FOREIGN KEY FK_729F519B28CDC89C');
        $this->addSql('DROP TABLE transaction');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE contract');
        $this->addSql('DROP TABLE contract_template');
        $this->addSql('DROP TABLE place');
        $this->addSql('DROP TABLE home');
        $this->addSql('DROP TABLE invoice');
        $this->addSql('DROP TABLE owner');
        $this->addSql('DROP TABLE room');
        $this->addSql('DROP TABLE tenant');
        $this->addSql('DROP TABLE ext_translations');
        $this->addSql('DROP TABLE ext_log_entries');
    }
}

<?php

namespace App\Repository;

use App\Entity\Contract;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Contract|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contract|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contract[]    findAll()
 * @method Contract[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContractRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Contract::class);
    }

    public function findMostRecentTenantContractByRoom($roomId){
        $qb = $this->createQueryBuilder('c')
                ->innerJoin('c.person','cp')
                ->innerJoin('c.place','cr')
                ->where('cr.placeId = :roomId')
                ->andWhere('c.endDate > :now')
                ->orderBy('c.endDate','DESC');

        $qb->setParameter('roomId',$roomId)
           ->setParameter('now', date('Y-m-d H:i:s'));

        return $qb->getQuery()->getResult();
    }

    public function findAboutToExpire(){
        $qb = $this->createQueryBuilder('c')
                ->where(':now BETWEEN c.startDate AND c.endDate')
                ->andWhere('MONTH(c.endDate) = :now_month')
                ->andWhere('YEAR(c.endDate) = :now_year')
                ->orderBy('c.endDate','DESC');

        $qb->setParameter('now',date('Y-m-d H:i:s'))
           ->setParameter('now_month',date('n'))
           ->setParameter('now_year',date('Y'));



        return $qb->getQuery()->getResult();
    }

    public function findNotRenewing(){

        $currentDate = new \DateTime("now");
        $thirtyDaysLater = $currentDate->modify("+30 days");
        $qb = $this->createQueryBuilder('c')
                ->where('c.actualEndDate < :thirtyDaysLater')
                ->orderBy('c.endDate','DESC');

         $qb->setParameter('thirtyDaysLater',$thirtyDaysLater->format('Y-m-d H:i:s'));

        return $qb->getQuery()->getResult();
    }

    public function findExpired(){
        $qb = $this->createQueryBuilder('c')
                ->where('c.endDate < :now')
                ->andWhere('c.actualEndDate is null'); 

        $qb->setParameter('now',date('Y-m-d H:i:s'));


        return $qb->getQuery()->getResult();
    }

    public function findActive(){
        $qb = $this->createQueryBuilder('c')
                ->where(':now BETWEEN c.startDate AND c.endDate')                
                ->andWhere("c.contractType = 'Tenant Rental Aggrement'")
                ->orderBy('c.endDate','DESC');

        $qb->setParameter('now',date('Y-m-d H:i:s'));



        return $qb->getQuery()->getResult();

    }


//    /**
//     * @return Contract[] Returns an array of Contract objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Contract
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

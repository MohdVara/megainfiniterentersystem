<?php

namespace App\Repository;

use App\Entity\Room;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Room|null find($id, $lockMode = null, $lockVersion = null)
 * @method Room|null findOneBy(array $criteria, array $orderBy = null)
 * @method Room[]    findAll()
 * @method Room[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Room::class);
    }

    public function selectAllRoomWithCountTenants()
    {

        $qb = $this->createQueryBuilder('r')
                ->select(array('r.placeId','r.number','r.roomType','r.capacity','r.deletedAt','rh.number','rh.homeAddress','COUNT(DISTINCT rcp) AS tenantTotal'))
                ->innerJoin('r.home','rh')
                ->innerJoin('r.contracts','rc')
                ->innerJoin('rc.person','rcp')
                ->where('rc.startDate < :now')
                ->andWhere('rc.endDate > :now')
                ->setParameter('now', date('Y-m-d H:i:s'))
                ->groupBy('r');

        return $qb->getQuery()->getResult();
    }

//    /**
//     * @return Room[] Returns an array of Room objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Room
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

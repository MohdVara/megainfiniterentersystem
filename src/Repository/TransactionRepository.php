<?php

namespace App\Repository;

use App\Entity\Transaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Transaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    public function getCurrentMonthByPerson($personId){
       
        $qb = $this->createQueryBuilder('t')
                ->where('t.personId = :personId')
                ->andWhere('MONTH(t.dateIssued) = :now_month')
                ->andWhere('YEAR(t.dateIssued) = :now_year');

        $qb->setParameter('personId',$personId)
           ->setParameter('now_month',date('n'))
           ->setParameter('now_year', date('Y'));

        return $qb->getQuery()->getResult();
    }

    public function getMonthlyRentRecordByPerson($personId){
        $qb = $this->createQueryBuilder('t')
                ->where('t.personId = :personId')
                ->andWhere('t.transactionDescription = CONCAT("Sewa Bulan ",:now_month,"-",:now_year)');

        $qb->setParameter('personId',$personId)
           ->setParameter('now_month',date('n'))
           ->setParameter('now_year', date('Y'));

        return $qb->getQuery()->getResult();
    }
    
    public function getPenaltyByPerson($personId){
        $qb = $this->createQueryBuilder('t')
                ->where('t.personId = :personId')
                ->andWhere('t.transactionDescription = CONCAT("Denda Bulan ",:now_month,"-",:now_year)');

        $qb->setParameter('personId',$personId)
           ->setParameter('now_month',date('n'))
           ->setParameter('now_year', date('Y'));

        return $qb->getQuery()->getResult();
    } 

//    /**
//     * @return Transaction[] Returns an array of Transaction objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Transaction
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
